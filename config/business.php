<?php

return [
    'vat'                 => env('VAT', 24),

    'name'                => env('BUSINESS_NAME'),
    'headquarters'        => env('BUSINESS_HEADQUARTERS'),
    'address'             => env('BUSINESS_ADDRESS'),
    'phone'               => env('BUSINESS_PHONE'),
    'vin'                 => env('BUSINESS_VIN'),
    'registration_number' => env('BUSINESS_REGISTRATION_NUMBER'),
    'tax_office'          => env('BUSINESS_TAX_OFFICE'),
];
