<?php

return [
    'restore_entrance_from_last_seconds' => env('RESTORE_ENTRANCE_FROM_LAST_SECONDS', 10),
    'restore_exit_from_last_seconds'     => env('RESTORE_EXIT_FROM_LAST_SECONDS', 10),

    'purchase' => [
        'default' => env('TRANSITS_PURCHASE_DEFAULT', 10),
        'max'     => env('TRANSITS_PURCHASE_MAX', 20),
    ],

];
