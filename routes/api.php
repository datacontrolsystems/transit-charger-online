<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('info/time', 'API\InfoController@time')->name('info.time');

Route::middleware('auth:api')->group(function () {

    Route::get('user/current', 'API\UserController@currentUser')->name('user.current');

    Route::get('vehicle-entrance/request/{card_code}', 'API\VehicleEntranceController@request')->name('vehicle-entrance.request');
    Route::get('vehicle-exit/request/{card_code}', 'API\VehicleExitController@request')->name('vehicle-exit.request');

});
