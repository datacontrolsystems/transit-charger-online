Transit Charger Online
======================

[Terminals' API Doc](https://documenter.getpostman.com/view/1408338/UVeJKQVc)


# Stripe 

Πρέπει να υπάρχει **μόνο ένα ενεργό** προϊόν με metadata `product => transit`.
Αυτό το προϊόν πρέπει να περιλαμβάνει **μόνο μία ενεργή τιμή**.

Δημιουργία webhook.

# Import Terminals

Entrance Terminals `database/entrance_terminals.csv`

```csv
name,api_token
termIn1,sXOUQBLCuQNu3vNbPF7HI8nvMJ9ViT23Cu5vD2yJOgEkWsFBEsknSjuPDm4k
```

Exit Terminals `database/exit_terminals.csv`

```csv
name,api_token
termOut1,PJzrZZJkcdDaPGSGZbv0j2fNeqy4QBlRPUZhKL7IvMtzkGxtgShURr2LwBn1
```

# Migration from old Transit Charger

Export to `database/customers.csv`

```sql
SELECT customers.*, c1.rfid AS card_code, c1.plate AS plate
FROM customers
         JOIN charges c1 ON customers.id = c1.customer_id
         LEFT JOIN charges c2 ON (customers.id = c2.customer_id AND c1.id < c2.id)
WHERE c2.id IS NULL;
```
