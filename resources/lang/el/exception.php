<?php
/**
 * exception.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 2019-02-04 11:38
 */

return [
    'AvailableSubscriptionNotFound' => 'Δεν βρέθηκε ενεργή συνδρομή για αυτό το όχημα για την ζώνη στάθμευσης ή έχουν καλυφθεί οι κρατημένες θέσεις.',
    'CardExtraChargesNotPaid'       => 'Υπάρχει επιπλέον χρέωση κάρτας που δεν πληρώθηκε.',
    'CustomerInactive'              => 'Ανενεργός Πελάτης',
    'CustomerInDebt'                => 'Ο Πελάτης έχει υπερβεί το μέγιστο υπόλοιπο.',
    'DateActivation'                => 'Εκτός καθορισμένης περιόδου',
    'DifferentVehicle'              => 'Το όχημα είναι διαφορετικό από το καθορισμένο.',
    'InsufficientReservedSpots'     => 'Έχουν καλυφθεί οι κρατημένες θέσεις της συνδρομής.',
    'InsufficientSubscription'      => 'Εκτός περιόδου συνδρομής',
    'InvalidVehicle'                => 'Το όχημα δεν ταιριάζει.',
    'ParkingZoneFull'               => 'Η ζώνη στάθμευσης είναι πλήρης.',
    'SubscriptionNotRenewable'      => 'Μη ανανεώσιμη συνδρομή',
    'TicketAlreadyExited'           => 'Το εισιτήριο έχει εξέλθει.',
    'TicketAlreadyPaid'             => 'Το εισιτήριο είναι πληρωμένο.',
    'TicketNotPaid'                 => 'Το εισιτήριο δεν έχει πληρωθεί.',
    'TicketPricingCalculation'      => 'Πρόβλημα κοστολόγησης εισιτηρίου.',
    'UnknownVehicle'                => 'Μη αναγνωρισμένο όχημα',
    'VehicleAlreadyEntered'         => 'Το όχημα έχει εισέλθει.',
    'VehicleAlreadyExited'          => 'Το όχημα έχει εξέλθει.',
    'InvalidInvoice'                => 'Αδυναμία Παραστατικού',
    'Socket'                        => 'Σφάλμα Επικοινωνίας',
    'InvalidPaymentMethod'          => 'Μη έγκυρος τρόπος πληρωμής',
    'InvalidData'                   => 'Μη Έγκυρες Πληροφορίες',
    'Booking'                       => 'Αποτυχία Κράτησης',
    'UsedBooking'                   => 'Η κράτηση έχει χρησιμοποιηθεί ήδη.',
    'ExpiredBooking'                => 'Η κράτηση έχει λήξει.',
    'CancelledBooking'              => 'Η κράτηση έχει ακυρωθεί.',
    'UnpaidBooking'                 => 'Η κράτηση δεν έχει πληρωθεί.',
    'ExitWithoutEntrance'           => 'Έξοδος χωρίς είσοδο',
    'BookingAlreadyPaid'            => 'Κράτηση ήδη πληρωμένη',
    'InvalidParkingZone'            => 'Μη έγκυρη Ζώνη Στάθμευσης',
    'ChargeCalculation'             => 'Σφάλμα Υπολογισμού Χρέωσης',
];
