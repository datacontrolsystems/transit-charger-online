<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChargesTable extends Migration
{
    public function up()
    {
        Schema::create('charges', function (Blueprint $table) {
            $table->id();
            $table->integer('transits');
            $table->string('payment_status');
            $table->foreignId('customer_id')->constrained();
            $table->string('stripe_checkout_id')->nullable()->unique();
            $table->json('parameters');
            $table->timestamps();
            $table->blameable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('charges');
    }
}
