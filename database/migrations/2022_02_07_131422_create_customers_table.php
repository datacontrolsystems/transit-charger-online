<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('card_code')->unique();
            $table->string('plate')->unique();
            $table->integer('remaining_transits')->default(0);
            $table->string('phone')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('tin')->nullable()->unique();
            $table->string('profession')->nullable();
            $table->string('tax_office')->nullable();
            $table->string('address')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->json('parameters');
            $table->foreignId('user_id')->constrained();
            $table->timestamps();
            $table->softDeletes();
            $table->blameable();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreignId('customer_id')->nullable()->constrained();
        });
    }

    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
