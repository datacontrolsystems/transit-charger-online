<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleEntrancesTable extends Migration
{
    public function up()
    {
        Schema::create('vehicle_entrances', function (Blueprint $table) {
            $table->id();
            $table->dateTime('entered_at');
            $table->dateTime('exited_at')->nullable();
            $table->foreignId('entrance_terminal_id')->constrained('users');
            $table->foreignId('customer_id')->constrained();
            $table->json('parameters');
            $table->timestamps();
            $table->blameable();
            $table->softDeletes();
            $table->boolean('manually')->storedAs('ENTRANCE_TERMINAL_ID <> CREATED_BY');
        });
    }

    public function down()
    {
        Schema::dropIfExists('vehicle_entrances');
    }
}
