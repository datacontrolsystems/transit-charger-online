<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleExitsTable extends Migration
{
    public function up()
    {
        Schema::create('vehicle_exits', function (Blueprint $table) {
            $table->id();
            $table->dateTime('exited_at');
            $table->foreignId('exit_terminal_id')->constrained('users');
            $table->foreignId('customer_id')->constrained();
            $table->foreignId('vehicle_entrance_id')->constrained();
            $table->json('parameters');
            $table->timestamps();
            $table->softDeletes();
            $table->blameable();
            $table->boolean('manually')->storedAs('EXIT_TERMINAL_ID <> CREATED_BY');
        });
    }

    public function down()
    {
        Schema::dropIfExists('vehicle_exits');
    }
}
