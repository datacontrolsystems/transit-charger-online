<?php

namespace Database\Seeders;

use App\Imports\ExitTerminalsImport;
use App\Models\ExitTerminal;
use Illuminate\Database\Seeder;

class ExitTerminalsSeeder extends Seeder
{

    public function run()
    {
        ExitTerminal::withTrashed()->firstOrCreate(['username' => 'irregularExitTerminal'], ['name' => 'Ακανόνιστο Τερματικό Εξόδου']);

        $file = database_path('exit_terminals.csv');
        if (valid_file($file)) {
            (new ExitTerminalsImport())->import($file);
        }
    }

}
