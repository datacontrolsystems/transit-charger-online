<?php

namespace Database\Seeders;

use App\Imports\EntranceTerminalsImport;
use Illuminate\Database\Seeder;

class EntranceTerminalsSeeder extends Seeder
{
    public function run()
    {
        $file = database_path('entrance_terminals.csv');
        if (valid_file($file)) {
            (new EntranceTerminalsImport)->import($file);
        }
    }
}
