<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DefaultSettingsSeeder extends Seeder
{
    public function run()
    {
        setting(collect(config('setting.override'))->flip()->map('config')->toArray())->save();
    }
}
