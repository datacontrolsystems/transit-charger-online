<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class DefaultRolesWithPermissionsSeeder extends Seeder
{
    /**
     * @var Role
     */
    protected $superAdmin;
    /**
     * @var Role
     */
    protected $admin;

    public function run()
    {
        $this->setSuperAdminPermissions();
        $this->setAdminPermissions();
        $this->setCustomerPermissions();
    }

    protected function setSuperAdminPermissions()
    {
        $this->superAdmin = Role::superAdmin()->syncPermissions(Permission::all());
    }

    protected function setAdminPermissions()
    {
        $permissions = flatten_permissions_map([
            'view'    => DefaultPermissionsSeeder::$models,
            'viewAny' => DefaultPermissionsSeeder::$models,
            'create'  => $editable = [
                'User',
                'Customer',
            ],
            'update'  => $editable,
            'delete'  => $editable,
            'admin',
            'run commands',
            'credit-transits',
        ]);
        $this->admin = Role::admin()->syncPermissions($permissions);
    }

    protected function setCustomerPermissions()
    {
        $permissions = flatten_permissions_map([
            'viewAny' => [
                'Customer',
                'Charge',
                'VehicleEntrance',
                'VehicleExit',
            ],
        ]);
        Role::customer()->syncPermissions($permissions);
    }
}
