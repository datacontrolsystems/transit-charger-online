<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

class DefaultPermissionsSeeder extends Seeder
{
    /**
     * view -> always true
     * forceDelete -> always false
     * @var array
     */
    protected $actions = Permission::ACTIONS;

    public static $models = ['Customer', 'VehicleEntrance', 'VehicleExit', 'EntranceTerminal', 'ExitTerminal', 'Charge', 'User', 'Permission', 'Role', 'Note', 'ActionEvent'];

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $remove_abilities = [
        'create'  => [
            'ActionEvent',
            'VehicleExit',
            'VehicleEntrance',
        ],
        'update'  => [
            'ActionEvent',
        ],
        'delete'  => [
            'ActionEvent',
            'VehicleExit',
            'VehicleEntrance',
        ],
        'restore' => [
            'ActionEvent',
        ],
    ];

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $relation_actions = [];

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $abilities = [
        'run commands'    => true,
        'super-admin'     => true,
        'admin'           => true,
        'credit-transits' => true,
    ];

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $permissions = [];

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $nova_actions = [
//        'OpenBarrier'          => ['', 'EntranceTerminal', 'ExitTerminal'],
    ];

    protected $lowercase = false;

    public function __invoke(array $parameters = [])
    {
        $this->permissions = collect($this->permissions);
        $this->abilities = collect($this->abilities);
        $this->remove_abilities = collect($this->remove_abilities);
        $this->relation_actions = collect($this->relation_actions);
        $this->nova_actions = collect($this->nova_actions);
        return parent::__invoke();
    }

    public function run()
    {
        if (!Auth::user()) {
            Auth::loginUsingId(User::system()->id);
        }
        $this->buildStandardAbilities();
        $this->addRelationActions();
        $this->addNovaAbilities();
        $this->removeAbilities();
        $this->save();
    }

    private function save()
    {
        $this->abilities->each(function ($enabled, $ability) {
            $name = $this->lowercase ? mb_strtolower($ability) : $ability;
            if ($enabled) {
                $p = \App\Models\Permission::updateOrCreate(['name' => $name]);
                $this->permissions->push($p);
            } else {
                // delete ability if exists
                // used during updates
                try {
                    $p = \App\Models\Permission::findByName($name);
                    $p->forceDelete();
                } catch (\Spatie\Permission\Exceptions\PermissionDoesNotExist|\Spatie\Permission\Exceptions\PermissionDoesNotExist $e) {
                }
            }
        });
    }

    private function buildStandardAbilities()
    {
        foreach ($this->actions as $action)
            foreach (static::$models as $model)
                $this->abilities->put("$action $model", true);
    }

    private function removeAbilities()
    {
        $this->remove_abilities->each(function ($models, $action) {
            foreach ($models as $model)
                $this->abilities->put("$action $model", false);
        });
    }

    private function addRelationActions()
    {
        $this->relation_actions->each(function ($ra) {
            $this->abilities->put($ra, true);
        });
    }

    private function addNovaAbilities()
    {
        $this->nova_actions->each(function ($models, $action) {
            foreach ($models as $model) {
                $this->abilities->put("$action $model", true);
            }
        });
    }

}
