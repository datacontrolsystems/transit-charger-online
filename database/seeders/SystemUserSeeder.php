<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class SystemUserSeeder extends Seeder
{
    public function run()
    {
        User::insert([
            'id'                => 1,
            'name'              => 'System',
            'username'          => 'system',
            'email'             => 'system@' . str_slug(config('app.name')) . '.app',
            'password'          => bcrypt('DataControlS'),
            'type'              => 'user',
            'created_by'        => 1,
            'email_verified_at' => now(),
            'created_at'        => now(),
            'parameters'        => '{}',
        ]);
        User::insert([
            'id'                => 2,
            'name'              => __('Cron Scheduler'),
            'username'          => 'cron',
            'email'             => 'cron@' . str_slug(config('app.name')) . '.app',
            'password'          => bcrypt('DataControlS'),
            'type'              => 'user',
            'created_by'        => 1,
            'email_verified_at' => now(),
            'created_at'        => now(),
            'parameters'        => '{}',
        ]);
    }
}
