<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class DefaultUsersSeeder extends Seeder
{
    public function run()
    {
        $superAdmin = Role::superAdmin();
        User::system()->assignRole($superAdmin);
        User::cron()->assignRole($superAdmin);
        collect([
            ['name' => 'Θεοφάνης Βαρδάτσικος', 'username' => 'theofanis', 'email' => 'vardtheo@gmail.com'],
            ['name' => 'Γιώργος Μαραγκάκης', 'username' => 'maragkakis', 'email' => 'gmaragkakis@dcs.com.gr'],
            ['name' => 'Διονύσης Κοζάκης', 'username' => 'kozakis', 'email' => 'dkozakis@virtual-net.gr'],
            ['name' => 'Χριστόδουλος Προβελέγκιος', 'username' => 'cprovelengios', 'email' => 'cprovelengios@gmail.com'],
            ['name' => 'Νεκτάριος Γκουβούσης', 'username' => 'ngouvousis', 'email' => 'ngouvousis@virtual-net.gr'],
            ['name' => 'Ταμίας 1', 'username' => 'tamias1', 'plain_password' => '123456', 'role' => Role::admin()],
        ])->each(function ($attrs) use ($superAdmin) {
            $role = array_pull($attrs, 'role', $superAdmin);
            $u = User::create($attrs + ['plain_password' => 'DataControlS']);
            $u->assignRole($role);
        });
    }

}
