<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class DefaultRolesSeeder extends Seeder
{
    public function run()
    {
        collect([
            ['name' => 'super-admin',],
            ['name' => 'admin',],
            ['name' => 'customer',],
        ])->each(function ($attrs) {
            Role::firstOrCreate($attrs);
        });
    }

}
