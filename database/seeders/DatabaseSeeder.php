<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(SystemUserSeeder::class);
        Auth::loginUsingId(User::system()->id);
        $this->call([
            DefaultRolesSeeder::class,
            DefaultPermissionsSeeder::class,
            DefaultRolesWithPermissionsSeeder::class,
            DefaultUsersSeeder::class,
            ExitTerminalsSeeder::class,
            EntranceTerminalsSeeder::class,
            CustomerSeeder::class,
            DefaultSettingsSeeder::class,
        ]);
        setting(['installed_at' => now()])->save();
        Auth::logout();
    }
}
