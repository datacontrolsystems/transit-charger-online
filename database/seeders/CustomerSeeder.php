<?php

namespace Database\Seeders;

use App\Imports\CustomersImport;
use App\Models\Customer;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class CustomerSeeder extends Seeder
{
    public function run()
    {
        $file = database_path('customers.csv');
        if (valid_file($file)) {
            (new CustomersImport)->import($file);
        } else if (App::isLocal()) {
            Customer::factory()->count(20)->create();
        }
    }
}
