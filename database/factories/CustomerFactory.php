<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerFactory extends Factory
{
    public function definition()
    {
        return [
            'name'               => $this->faker->name,
            'card_code'          => $this->faker->uuid,
            'remaining_transits' => $this->faker->numberBetween(0, 10),
            'plate'              => $this->faker->bothify('???#####'),
            'phone'              => $this->faker->phoneNumber,
            'email'              => $this->faker->email,
            'tin'                => $this->faker->numerify('#########'),
            'city'               => $this->faker->city,
            'address'            => $this->faker->streetAddress,
            'postal_code'        => $this->faker->postcode,
        ];
    }
}
