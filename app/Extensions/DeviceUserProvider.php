<?php

namespace App\Extensions;


use App\Scopes\UserTypeScope;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;

class DeviceUserProvider extends EloquentUserProvider implements UserProvider
{

    public function __construct(HasherContract $hasher)
    {
        parent::__construct($hasher, \App\Models\User::class);
    }

    protected function newModelQuery($model = null)
    {
        return parent::newModelQuery($model)->withoutGlobalScope(UserTypeScope::class);
    }

}
