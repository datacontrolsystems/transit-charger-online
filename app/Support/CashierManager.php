<?php

namespace App\Support;

use App\Exceptions\CriticalException;
use Closure;
use Illuminate\Cache\TaggedCache;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Laravel\Cashier\Cashier;

class CashierManager
{
    public const TRANSIT_PRODUCT = 'transit';

    public function cache(): TaggedCache
    {
        return Cache::tags(static::class);
    }

    private function remember(string $key, Closure $callback)
    {
        return $this->cache()->remember($key, 86400, $callback);
    }

    public function transitProduct(): \Stripe\Product
    {
        return $this->remember(__FUNCTION__, function () {
            return collect(Cashier::stripe()->products->all(['active' => true])->data)
                ->where('metadata.product', self::TRANSIT_PRODUCT)
                ->tap(function (Collection $products) {
                    throw_if($products->count() > 1, CriticalException::class, "Multiple transit stripe products found");
                    throw_if($products->isEmpty(), CriticalException::class, "Transit stripe product not found");
                })
                ->firstOrFail();
        });
    }

    public function transitPrice(): \Stripe\Price
    {
        return $this->remember(__FUNCTION__, function () {
            return collect(Cashier::stripe()->prices->all(['active' => true, 'product' => $this->transitProduct()->id])->data)
                ->tap(function (Collection $prices) {
                    throw_if($prices->count() > 1, CriticalException::class, 'Multiple transit stripe prices found');
                    throw_if($prices->isEmpty(), CriticalException::class, 'Transit stripe price not found');
                })
                ->firstOrFail();
        });
    }

}
