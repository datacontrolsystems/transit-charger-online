<?php

namespace App\Support;

class Vat
{
    /**
     * @return int 24
     */
    public static function percentage(): int
    {
        return (int)config('business.vat');
    }

    /**
     * @return float 0.24
     */
    public static function decimal(): float
    {
        return static::percentage() / 100;
    }

    /**
     * @return float 1.24
     */
    public static function elevatedDecimal(): float
    {
        return 1 + static::decimal();
    }

    /**
     * @param $net_amount
     * @return float The total amount
     */
    public static function addTo($net_amount)
    {
        return $net_amount * self::elevatedDecimal();
    }

    /**
     * @param $total_amount
     * @return float The net amount
     */
    public static function removeFrom($total_amount)
    {
        return $total_amount / self::elevatedDecimal();
    }

    /**
     * @param $total_amount
     * @return float The vat value
     */
    public static function getFrom($total_amount)
    {
        return $total_amount - self::removeFrom($total_amount);
    }

}
