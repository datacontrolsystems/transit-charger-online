<?php

namespace App\Support;


use App\Exceptions\InvalidDataException;
use App\Imports\TaxOfficesImport;
use Illuminate\Support\Facades\Cache;

class TaxOffices
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public static function parse()
    {
        return Cache::tags(__CLASS__)->remember('TaxOffices-parse', config('cache.duration'), function () {
            return collect((new TaxOfficesImport)->toArray(database_path('tax_offices.csv'))[0]);
        });
    }

    /**
     * @return array|\Illuminate\Support\Collection
     */
    public static function list()
    {
        return Cache::tags(__CLASS__)->remember('TaxOffices-list', config('cache.duration'), function () {
            return static::parse()->sortBy('name')->keyBy('name')->map->name;
        });
    }

    /**
     * @param string $tax_office
     * @return string the checked tax office
     * @throws InvalidDataException
     */
    public static function validate(string $tax_office)
    {
        throw_unless(static::list()->contains($tax_office), InvalidDataException::class, "Invalid tax office '$tax_office'.");
        return $tax_office;
    }

    public static function findByCode($code): array
    {
        return static::parse()->where('code', $code)->firstOrFail();
    }
}
