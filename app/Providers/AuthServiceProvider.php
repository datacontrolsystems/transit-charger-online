<?php

namespace App\Providers;

use App\Extensions\DeviceUserProvider;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        \App\Models\User::class                => \App\Policies\UserPolicy::class,
        \App\Models\EntranceTerminal::class    => \App\Policies\EntranceTerminalPolicy::class,
        \App\Models\ExitTerminal::class        => \App\Policies\ExitTerminalPolicy::class,
        \App\Models\VehicleEntrance::class     => \App\Policies\VehicleEntrancePolicy::class,
        \App\Models\VehicleExit::class         => \App\Policies\VehicleExitPolicy::class,
        \App\Models\Charge::class              => \App\Policies\ChargePolicy::class,
        \App\Models\Permission::class          => \App\Policies\PermissionPolicy::class,
        \App\Models\Role::class                => \App\Policies\RolePolicy::class,
        \App\Models\Actions\ActionEvent::class => \App\Policies\ActionEventPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->defineGates();
        $this->defineProviders();
    }

    private function defineGates()
    {
        Gate::define('entrance-terminal', function (User $user) {
            return $user instanceof \App\Models\EntranceTerminal;
        });
        Gate::define('exit-terminal', function (User $user) {
            return $user instanceof \App\Models\ExitTerminal;
        });

        Gate::define('system', function (User $user) {
            return in_array($user->username, ['system', 'cron']);
        });

        Gate::define('instanceof', function (User $user, $class) {
            if (!class_exists($class)) {
                $class = app('models')[studly_case($class)];
            }
            return $user instanceof $class;
        });
    }

    private function defineProviders()
    {
        Auth::provider('device', function ($app, array $config) {
            return new DeviceUserProvider($this->app['hash']);
        });
    }
}
