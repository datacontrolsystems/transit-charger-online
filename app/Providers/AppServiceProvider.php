<?php

namespace App\Providers;

use App\Models\Customer;
use App\Support\CashierManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;
use Laravel\Cashier\Cashier;
use SplFileInfo;

class AppServiceProvider extends ServiceProvider
{

    public function register()
    {
        Cashier::ignoreMigrations();
    }

    public function boot()
    {
        $this->setupBindings();
        $this->findModels();
        $this->findPolicies();
        $this->findObservers();
        $this->findExceptions();
//        $this->registerObservers();
        $this->registerFileMacros();
        $this->configureBlameable();
        $this->customizeCarbon();
        Cashier::useCustomerModel(Customer::class);
        JsonResource::withoutWrapping();
    }

    private function setupBindings()
    {
        $this->app->singleton('installed', function () {
            return config('app.installed_at');
        });
        $this->app->bind('cashier-manager', function (){
            return new CashierManager();
        });
    }

    private function customizeCarbon()
    {
        Carbon::serializeUsing(function ($date) {
            return $date->format('Y-m-d H:i:s');
        });
    }

    protected function registerFileMacros()
    {
        File::macro('latestFile',
            /**
             * @param string $pattern
             * @return SplFileInfo|null
             */
            function ($pattern) {
                $files = collect(File::glob($pattern))->mapWithKeys(function ($file) {
                    $f = new SplFileInfo($file);
                    return [$f->getCTime() => $f];
                });
                return $files->get($files->keys()->max());
            }
        );
        File::macro('deleteFilesByPattern',
            /**
             * @param string $pattern
             * @param array $exclusions - Pathname of files to exclude
             * @return \Illuminate\Support\Collection|SplFileInfo[] The deleted files
             */
            function ($pattern, $exclusions = []) {
                return collect(File::glob($pattern))->map(function ($file) use ($exclusions) {
                    return in_array($file, $exclusions) ?
                        false :
                        (($file = new SplFileInfo($file))->isFile() ?
                            File::delete($file->getPathname()) :
                            false);
                })->filter();
            }
        );
        File::macro('generateTempFilename', function () {
            return tempnam(sys_get_temp_dir(), str_slug(config('app.name')));
        });
        File::macro('storeTempFile', function ($data) {
            $filename = File::generateTempFilename();
            if (File::put($filename, $data)) {
                return $filename;
            }
            return false;
        });
    }

    private function configureBlameable()
    {
        Blueprint::macro('blameable', function () {
            $this->foreignId(config('blameable.column_names.createdByAttribute'))->nullable();
            $this->foreignId(config('blameable.column_names.updatedByAttribute'))->nullable();
            $this->foreignId(config('blameable.column_names.deletedByAttribute'))->nullable();
        });
    }

    protected function registerObservers()
    {
        app('observers')->each(function ($observer, $model) {
            $model::observe($observer);
        });
    }

    protected function findModels()
    {
        $this->app->singleton('models', function () {
            return Cache::remember('app-models', config('cache.duration'), function () {
                $modelNamespace = 'Models';
                $appNamespace = \Illuminate\Container\Container::getInstance()->getNamespace();
                return $models = collect(\File::allFiles(app_path($modelNamespace)))->mapWithKeys(function (\SplFileInfo $item) use ($appNamespace, $modelNamespace) {
                    $rel = $item->getRelativePathName();
                    $class = sprintf('%s%s%s', $appNamespace, $modelNamespace ? $modelNamespace . '\\' : '',
                        implode('\\', explode('/', substr($rel, 0, strrpos($rel, '.')))));
                    try {
                        $class = new \ReflectionClass($class);
                        return [
                            $class->getShortName() =>
                                (!$class->isAbstract() && $class->isSubclassOf(Model::class)) ? $class->getName() : null,
                        ];
                    } catch (\ReflectionException $e) {
                        return [];
                    }
                })->filter();
            });
        });
    }

    protected function findExceptions()
    {
        $this->app->singleton('exceptions', function () {
            $modelNamespace = 'Exceptions';
            $appNamespace = \Illuminate\Container\Container::getInstance()->getNamespace();
            return collect(\File::allFiles(app_path($modelNamespace)))->mapWithKeys(function (\SplFileInfo $item) use ($appNamespace, $modelNamespace) {
                $rel = $item->getRelativePathName();
                $class = sprintf('%s%s%s', $appNamespace, $modelNamespace ? $modelNamespace . '\\' : '',
                    implode('\\', explode('/', substr($rel, 0, strrpos($rel, '.')))));
                try {
                    $class = new \ReflectionClass($class);
                    return [
                        $class->getShortName() =>
                            (!$class->isAbstract() && $class->isSubclassOf(\App\Exceptions\BaseException::class)) ? $class->getName() : null,
                    ];
                } catch (\ReflectionException $e) {
                    return [];
                }
            })->filter();
        });
    }

    protected function findPolicies()
    {
        $this->app->singleton('policies', function () {
            return Cache::remember('app-policies', config('cache.duration'), function () {
                $models = app('models');
                $modelNamespace = 'Policies';
                $appNamespace = \Illuminate\Container\Container::getInstance()->getNamespace();
                return $models = collect(\File::allFiles(app_path($modelNamespace)))->mapWithKeys(function (\SplFileInfo $item) use ($models, $appNamespace, $modelNamespace) {
                    $rel = $item->getRelativePathName();
                    $class = sprintf('%s%s%s', $appNamespace, $modelNamespace ? $modelNamespace . '\\' : '',
                        implode('\\', explode('/', substr($rel, 0, strrpos($rel, '.')))));
                    return (class_exists($class) && !(new \ReflectionClass($class))->isAbstract()) ?
                        [$models[str_before(class_basename($class), 'Policy')] => $class] : [];
                })->filter();
            });
        });
    }

    protected function findObservers()
    {
        $this->app->singleton('observers', function () {
            return Cache::remember('app-observers', config('cache.duration'), function () {
                $models = app('models');
                $modelNamespace = 'Observers';
                $appNamespace = \Illuminate\Container\Container::getInstance()->getNamespace();
                return $models = collect(\File::allFiles(app_path($modelNamespace)))->mapWithKeys(function (\SplFileInfo $item) use ($models, $appNamespace, $modelNamespace) {
                    $rel = $item->getRelativePathName();
                    $class = sprintf('%s%s%s', $appNamespace, $modelNamespace ? $modelNamespace . '\\' : '',
                        implode('\\', explode('/', substr($rel, 0, strrpos($rel, '.')))));
                    return (class_exists($class) && !(new \ReflectionClass($class))->isAbstract()) ?
                        [$models[str_before(class_basename($class), 'Observer')] => $class] : [];
                })->filter();
            });
        });
    }

}
