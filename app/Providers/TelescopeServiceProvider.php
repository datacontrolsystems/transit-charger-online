<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Laravel\Telescope\EntryType;
use Laravel\Telescope\IncomingEntry;
use Laravel\Telescope\Telescope;
use Laravel\Telescope\TelescopeApplicationServiceProvider;

class TelescopeServiceProvider extends TelescopeApplicationServiceProvider
{
    public function register()
    {
        $this->hideSensitiveRequestDetails();
        $this->tag();
        $this->filter();
    }

    private function tag()
    {
        Telescope::tag(function (IncomingEntry $entry) {
            switch ($entry->type) {
                case EntryType::REQUEST:
                    $tags = $this->tagRequest($entry);
                    break;
                case EntryType::LOG:
                    $tags = $this->tagLog($entry);
                    break;
                case EntryType::EXCEPTION:
                    $tags = $this->tagException($entry);
                    break;
                case EntryType::CLIENT_REQUEST:
                    $tags = $this->tagClientRequest($entry);
                    break;
                default:
                    $tags = [];
            }

            $tags = collect($tags);
            $tags[] = now()->format('Y-m-d H');

            $request = request();
            //add to \App\Http\Middleware\EncryptCookies::$except
            $keys = ['TID'];

            foreach (['request', 'headers', 'cookies'] as $bag) {
                $tags = $tags->merge(
                    collect($request->$bag->all())
                        ->only($keys)
                        ->filter()
                        ->map(fn($v, $k) => "$k:$v")
                        ->values()

                );
            }

            return $tags;
        });
    }

    private function tagRequest(IncomingEntry $entry)
    {
        $tags = collect([
            "status:{$entry->content['response_status']}",
            "method:{$entry->content['method']}",
            "ip:{$entry->content['ip_address']}",
        ]);

        if ($route = Route::current()) {

            if ($route->named('api.*')) {
                $tags->push($route->getName(), ...explode('.', $route->getName()));

                if (array_get($entry->content, 'duration') > 2500) {
                    $tags->push('slow');
                }

                if ($route->named('api.vehicle-entrance.*')) {
                    $tags->push('entrance');
                } else if ($route->named('api.vehicle-exit.*')) {
                    $tags->push('exit');
                }

                if ($card_code = $route->originalParameter('card_code'))
                    $tags->push("card:$card_code");
            }
        }

        return $tags->toArray();
    }

    private function filter()
    {
        Telescope::filter(function (IncomingEntry $entry) {
            if ($this->app->environment('local')) {
                return true;
            }

            $filter = function () use ($entry) {
                switch ($entry->type) {
                    case EntryType::REQUEST:
                        if (array_get($entry->content, 'status') >= 400 || array_get($entry->content, 'duration') > 2500)
                            return true;
                        return !starts_with(array_get($entry->content, 'uri'), ['/nova-api/', '/nova-vendor/']);
                }
                return true;
            };

            return $entry->isReportableException() ||
                $entry->isFailedRequest() ||
                $entry->isFailedJob() ||
                $entry->isScheduledTask() ||
                $entry->hasMonitoredTag() ||
                $filter();
        });
    }

    protected function hideSensitiveRequestDetails()
    {
        if ($this->app->environment('local')) {
            return;
        }
        Telescope::hideRequestParameters(['_token', 'api_token']);
        Telescope::hideRequestHeaders([
            'cookie',
            'x-csrf-token',
            'x-xsrf-token',
        ]);
    }

    protected function gate()
    {
        Gate::define('viewTelescope', function (User $user) {
            return $user;
        });
    }

    private function tagException(IncomingEntry $entry)
    {
        return [
            str_before(class_basename($entry->content['class']), 'Exception'),
        ];
    }

    private function tagLog(IncomingEntry $entry)
    {
        return [
            $entry->content['level'],
        ];
    }

    private function tagClientRequest(IncomingEntry $entry)
    {
        $tags = collect([
            "status:" . array_get($entry->content, 'response_status'),
            "method:" . array_get($entry->content, 'method'),
        ]);

        return $tags;
    }

}
