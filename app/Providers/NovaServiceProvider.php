<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Laravel\Cashier\Cashier;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;
use Laravel\Nova\Panel;
use Nalingia\SimpleToolbarLink\SimpleToolbarLink;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    public function boot()
    {
        parent::boot();
//        Nova::translations(resource_path('lang/' . app()->getLocale() . '.json'));
        $this->settings();
    }

    protected function settings()
    {
        \Epigra\NovaSettings\NovaSettingsTool::addSettingsFields(function () {
            $whenNotSuperAdmin = function (Request $request) {
                return !$request->user()->can('super-admin');
            };

            return [
                new Panel(__('Information'), [
                    Text::make(__('Installed At'), 'installed_at')->readonly(),
                    Text::make('Stripe Key', function () {
                        return config('cashier.key');
                    })->readonly(),
                    Text::make(__('Charge Per Transit'), function () {
                        try {
                            $p = app('cashier-manager')->transitPrice();
                            return cents_to_eur($p->unit_amount) . ' ' . $p->currency;
                        } catch (\Exception $e) {
                            return "Error: {$e->getMessage()}";
                        }
                    })
                        ->readonly()
                        ->help('<a href="https://dashboard.stripe.com/prices/' . optional(rescue(fn() => app('cashier-manager')->transitPrice()))->id . '" target="_blank">Τροποποίηση στο Stripe</a>'),
                    Text::make('Υπόλοιπο προς είσπραξη', function () {
                        $b = Cashier::stripe()->balance->retrieve();
                        $available = cents_to_eur(collect($b->available)->sum('amount'));
                        $pending = cents_to_eur(collect($b->pending)->sum('amount'));
                        return "$available € + $pending €";
                    })
                        ->readonly()
                        ->help('Διαθέσιμο + Σε εκκρεμότητα'),
                ]),

                new Panel(__('Entrances'), [
                    Number::make(__('Restore Entrance From Last Seconds'), 'restore_entrance_from_last_seconds')
                        ->rules(['required', 'integer', 'min:0'])
                        ->min(0)->max(1000)->step(1),
                    Number::make(__('Restore Exit From Last Seconds'), 'restore_exit_from_last_seconds')
                        ->rules(['required', 'integer', 'min:0'])
                        ->min(0)->max(1000)->step(1),
                ]),
            ];
        });
    }

    protected function routes()
    {
        Nova::routes()
            ->withAuthenticationRoutes()
//                ->withPasswordResetRoutes()
            ->register();

        Route::domain(config('nova.domain', null))
            ->middleware(['web'])
            ->prefix(Nova::path())
            ->post('/login', [\App\Http\Controllers\Nova\LoginController::class, 'login'])
            ->name('nova.login');
    }

    protected function gate()
    {
        Gate::define('viewNova', function (User $user) {
            return $user;
        });
    }

    protected function cards()
    {
        return [
            \Richardkeep\NovaTimenow\NovaTimenow::make()->timezones([config('app.timezone')]),
        ];
    }

    protected function dashboards()
    {
        return [];
    }

    public function tools()
    {
        $tools = [
            \Epigra\NovaSettings\NovaSettingsTool::make()->canSeeWhen('admin'),
            SimpleToolbarLink::make()->name('Stripe')->to('https://dashboard.stripe.com/payments')->target('_blank')->canSeeWhen('admin')->icon('<svg class="sidebar-icon" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"><path stroke-linecap="round" stroke-linejoin="round" d="M3 10h18M7 15h1m4 0h1m-7 4h12a3 3 0 003-3V8a3 3 0 00-3-3H6a3 3 0 00-3 3v8a3 3 0 003 3z" /></svg>'),
            \BinaryBuilds\NovaAdvancedCommandRunner\CommandRunner::make()->canSeeWhen('run commands'),
            SimpleToolbarLink::make()->name(__('Account'))
                ->to(route('nova.customer.current'))
                ->canSee(function (Request $request) {
                    return $request->user()->customer_id;
                }),
        ];

        if (Auth::user()->can('super-admin')) {
            $tools = array_merge($tools, [
                \Infinety\Filemanager\FilemanagerTool::make(),
                \Davidpiesse\NovaPhpinfo\Tool::make(),
                \Spatie\BackupTool\BackupTool::make(),
                \Sbine\RouteViewer\RouteViewer::make(),
                SimpleToolbarLink::make()->name('Telescope')->to('/telescope')->target('telescope')->icon('<svg class="sidebar-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80 80"><path fill="var(--sidebar-icon)" d="M0 40a39.87 39.87 0 0 1 11.72-28.28A40 40 0 1 1 0 40zm34 10a4 4 0 0 1-4-4v-2a2 2 0 1 0-4 0v2a4 4 0 0 1-4 4h-2a2 2 0 1 0 0 4h2a4 4 0 0 1 4 4v2a2 2 0 1 0 4 0v-2a4 4 0 0 1 4-4h2a2 2 0 1 0 0-4h-2zm24-24a6 6 0 0 1-6-6v-3a3 3 0 0 0-6 0v3a6 6 0 0 1-6 6h-3a3 3 0 0 0 0 6h3a6 6 0 0 1 6 6v3a3 3 0 0 0 6 0v-3a6 6 0 0 1 6-6h3a3 3 0 0 0 0-6h-3zm-4 36a4 4 0 1 0 0-8 4 4 0 0 0 0 8zM21 28a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path></svg>'),
                SimpleToolbarLink::make()->name('Horizon')->to('/horizon')->target('horizon')->icon('<svg class="sidebar-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path fill="var(--sidebar-icon)" d="M3.5,17.6C1.3,15.7,0,12.9,0,10c0-2.8,1.1-5.3,2.9-7.1C6.8-1,13.2-1,17.1,2.9s3.9,10.2,0,14.1 C13.4,20.8,7.5,21,3.5,17.6L3.5,17.6z M2.7,10.6c1.1-1,1.9-2.3,4-2.3c3.3,0,3.3,3.3,6.7,3.3c2.1,0,2.9-1.3,4-2.3 c-0.3-4-3.9-7-7.9-6.7S2.4,6.6,2.7,10.6L2.7,10.6z"/></svg>'),
                SimpleToolbarLink::make()->name('Terminal API Docs')->to('https://documenter.getpostman.com/view/1408338/UVeJKQVc')->target('api')->icon('<svg xmlns="http://www.w3.org/2000/svg" class="sidebar-icon" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="var(--sidebar-icon)"><path stroke-linecap="round" stroke-linejoin="round" d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" /></svg>'),
            ]);
        }

        return $tools;
    }

    public function register()
    {
        //
    }
}
