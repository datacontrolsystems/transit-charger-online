<?php

namespace App\Providers;

use App\Models\Card;
use App\Models\Customer;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Laravel\Nova\Nova;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    protected $namespace = 'App\\Http\\Controllers';

    public function boot()
    {
        $this->configureRateLimiting();
        $this->configureRoutes();
        $this->configureBindings();
    }

    private function configureBindings()
    {
        //Route::pattern('card_code', '[0-9a-zA-Z]+');

        Route::bind('card_code', function ($value) {
            return Customer::where('card_code', $value)->first() ?? abort(404, "card_code '$value' not found");
        });
    }

    private function configureRoutes()
    {
        $this->routes(function () {

            Route::prefix('api')
                ->name('api.')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));

            Route::middleware('nova')
                ->name('nova.')
                ->prefix(Nova::path())
                ->group(function () {
                    Route::get('/customer/current', [\App\Http\Controllers\Nova\CustomerController::class, 'toCurrentCustomer'])->name('customer.current');
                });

            Route::middleware(['json'])
                ->post('stripe/webhook', [\App\Http\Controllers\StripeWebhookController::class, 'handleWebhook'])->name('cashier.webhook');
        });
    }

    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
