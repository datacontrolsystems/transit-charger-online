<?php

namespace App\Providers;

use App\Models\Charge;
use App\Models\Customer;
use App\Models\User;
use App\Models\VehicleEntrance;
use App\Models\VehicleExit;
use App\Observers\ChargeObserver;
use App\Observers\CustomerObserver;
use App\Observers\UserObserver;
use App\Observers\VehicleEntranceObserver;
use App\Observers\VehicleExitObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    public function boot()
    {
        $this->setupObservers();
    }

    private function setupObservers()
    {
        VehicleEntrance::observe(VehicleEntranceObserver::class);
        VehicleExit::observe(VehicleExitObserver::class);
        Customer::observe(CustomerObserver::class);
        User::observe(UserObserver::class);
        Charge::observe(ChargeObserver::class);
    }

}
