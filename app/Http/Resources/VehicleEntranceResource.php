<?php

namespace App\Http\Resources;

use App\Models\VehicleEntrance;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin VehicleEntrance
 * @property VehicleEntrance $resource
 */
class VehicleEntranceResource extends JsonResource
{

    public function toArray($request)
    {
        return array_merge($this->resource->attributesToArray(), [
            'customer' => $this->resource->customer->attributesToArray(),
        ]);
    }

}
