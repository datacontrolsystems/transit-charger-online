<?php

namespace App\Http\Resources;

use App\Models\VehicleExit;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin VehicleExit
 * @property VehicleExit $resource
 */
class VehicleExitResource extends JsonResource
{

    public function toArray($request)
    {
        return array_merge($this->resource->attributesToArray(), [
            'customer' => $this->resource->customer->attributesToArray(),
        ]);
    }

}
