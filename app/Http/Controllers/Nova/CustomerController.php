<?php

namespace App\Http\Controllers\Nova;

use App\Http\Controllers\Controller;
use App\Models\Charge;
use Illuminate\Http\Request;
use Laravel\Nova\Nova;

class CustomerController extends Controller
{
    public function toCurrentCustomer(Request $request)
    {
        if ($request->has('payment_status')) {
            $charge = Charge::findByStripeCheckoutId($request->get('session_id'));
            $customer_id = $charge->customer_id;
            $charge->payment_status = $request->get('payment_status');
            $charge->save();
        }

        $customer_id = $customer_id ?? $request->query('customer', $request->user()->customer_id);

        return redirect(Nova::path() . "/resources/customers/$customer_id");
    }
}
