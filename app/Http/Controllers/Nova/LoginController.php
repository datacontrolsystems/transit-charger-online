<?php

namespace App\Http\Controllers\Nova;

use Laravel\Nova\Nova;

class LoginController extends \Laravel\Nova\Http\Controllers\LoginController
{

    public function username()
    {
        return 'username';
    }

    public function redirectPath()
    {
        if (request()->user()->customer_id) {
            return route('nova.customer.current');
        }

        return Nova::path();
    }

}
