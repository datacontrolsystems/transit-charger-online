<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\VehicleExitResource;
use App\Models\Customer;
use Illuminate\Http\Request;

class VehicleExitController extends Controller
{

    public function request(Request $request, Customer $customer)
    {
        return new VehicleExitResource($request->user()->doVehicleExit($customer));
    }

}
