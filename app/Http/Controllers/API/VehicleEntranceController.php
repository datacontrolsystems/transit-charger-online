<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\VehicleEntranceResource;
use App\Models\Customer;
use Illuminate\Http\Request;

class VehicleEntranceController extends Controller
{

    public function request(Request $request, Customer $customer)
    {
        $this->authorize('entrance-terminal');
        return new VehicleEntranceResource($request->user()->doVehicleEntrance($customer));
    }

}
