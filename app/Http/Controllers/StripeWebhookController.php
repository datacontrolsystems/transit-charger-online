<?php

namespace App\Http\Controllers;

use Laravel\Cashier\Http\Controllers\WebhookController;

class StripeWebhookController extends WebhookController
{

    public function handleProductCreated($payload)
    {
        return $this->clearCashierCache();
    }

    public function handleProductUpdated($payload)
    {
        return $this->clearCashierCache();
    }

    public function handleProductDeleted($payload)
    {
        return $this->clearCashierCache();
    }

    public function handlePriceCreated($payload)
    {
        return $this->clearCashierCache();
    }

    public function handlePriceUpdated($payload)
    {
        return $this->clearCashierCache();
    }

    public function handlePriceDeleted($payload)
    {
        return $this->clearCashierCache();
    }

    public function handlePaymentMethodAttached($payload)
    {
        // required to update default payment method when customer pays using Billing Portal
        if (!($customer = $this->getUserByStripeId($payload['data']['object']['customer']))) {
            return $this->error("Broker for stripe_id not found.");
        }
        if ($customer->hasDefaultPaymentMethod()) {
            return $this->successMethod(['message' => 'Customer already has default payment method.']);
        }
        $customer->updateDefaultPaymentMethod($payload['data']['object']['id']);
        return $this->successMethod();
    }

    protected function clearCashierCache()
    {
        app('cashier-manager')->cache()->flush();
        return $this->successMethod("Cleared cashier cache");
    }

    /**
     * @param string|array $parameters
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    protected function successMethod($parameters = [])
    {
        if (is_string($parameters)) {
            $parameters = ['message' => $parameters];
        }

        return response()->json(array_merge(['status' => 'processed'], $parameters));
    }

    protected function missingMethod($parameters = [])
    {
        return response()->json(array_merge(['status' => 'unhandled'], $parameters), 206);
    }

    /**
     * @param string|array $message
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    protected function error($message, $status = 422)
    {
        return response()->json(is_string($message) ? ['error' => $message] : $message, $status);
    }
}
