<?php

namespace App\Observers;

use App\Models\Customer;

class CustomerObserver
{
    public function creating(Customer $customer)
    {
        $customer->createUser();
    }

    public function created(Customer $customer)
    {
        $customer->updateUser();
    }

    public function updated(Customer $customer)
    {
        $customer->updateUser();
    }

    public function deleted(Customer $customer)
    {
        $customer->user->delete();
    }

    public function restored(Customer $customer)
    {
        $customer->user->restore();
    }

}
