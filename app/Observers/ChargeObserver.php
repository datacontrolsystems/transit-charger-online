<?php

namespace App\Observers;

use App\Models\Charge;
use Illuminate\Support\Facades\DB;

class ChargeObserver
{

    public function saved(Charge $charge)
    {
        if ($charge->isDirty('payment_status')) {
            rescue(function () use ($charge) {
                $charge->syncFromStripe()->saveQuietly();
            });

            if ($charge->payment_success && !$charge->getParameter('transits_credited')) {
                $charge->customer->remaining_transits += $charge->transits;
                $charge->setParameter('transits_credited', now()->toDateTimeString());

                DB::transaction(function () use ($charge) {
                    $charge->customer->save();
                    $charge->saveQuietly();
                });
            }
        }
    }

}
