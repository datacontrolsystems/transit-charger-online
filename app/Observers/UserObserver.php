<?php

namespace App\Observers;

use App\Models\User;

class UserObserver
{

    public function deleting(User $user)
    {
        throw_unless($user->customer->deleted_at, \Exception::class, "Delete customer instead of user");
    }

    public function restoring(User $user)
    {
        throw_unless($user->customer->deleted_at, \Exception::class, "Restore customer instead of user");
    }

}
