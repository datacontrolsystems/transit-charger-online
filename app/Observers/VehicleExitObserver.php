<?php

namespace App\Observers;

use App\Models\VehicleExit;

class VehicleExitObserver
{

    public function created(VehicleExit $exit)
    {
        $exit->vehicle_entrance->exited_at = $exit->exited_at;
        $exit->vehicle_entrance->save();
    }

}
