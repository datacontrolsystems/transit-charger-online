<?php

namespace App\Models;

use App\Models\Traits\ExitTerminalActions;
use App\Models\Traits\ModelCaching;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Parental\HasParent;

/**
 * App\Models\ExitTerminal
 *
 * @property int $id
 * @property string $name
 * @property string|null $type
 * @property string|null $username
 * @property string|null $email
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $password
 * @property array $parameters
 * @property string|null $api_token
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property int|null $customer_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \App\Models\User|null $creator
 * @property-read \App\Models\Customer|null $customer
 * @property-read \App\Models\User|null $deleter
 * @property string|null $barrier_controller
 * @property-read string $debug_name
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-write mixed $plain_password
 * @property-read \App\Models\User|null $updater
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VehicleExit[] $vehicle_exits
 * @property-read int|null $vehicle_exits_count
 * @method static \Illuminate\Database\Eloquent\Builder|User createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal newQuery()
 * @method static \Illuminate\Database\Query\Builder|ExitTerminal onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal query()
 * @method static \Illuminate\Database\Eloquent\Builder|User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereParameters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExitTerminal whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|ExitTerminal withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ExitTerminal withoutTrashed()
 * @mixin \Eloquent
 */
class ExitTerminal extends Terminal
{
    use HasParent;
    use ExitTerminalActions;
    use ModelCaching;

    /**
     * @param $columns
     * @return ExitTerminal[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function all($columns = ['*'])
    {
        return static::cache(__FUNCTION__, function () {
            return parent::all();
        });
    }

    public static function irregular(): ExitTerminal
    {
        return ExitTerminal::query()->where('username', 'irregularExitTerminal')->firstOrFail();
    }

    public function vehicle_exits(): HasMany
    {
        return $this->hasMany(VehicleExit::class, 'exit_terminal_id');
    }

}
