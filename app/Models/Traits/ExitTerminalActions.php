<?php

namespace App\Models\Traits;

use App\Exceptions\VehicleNotEnteredException;
use App\Models\Customer;
use App\Models\ExitTerminal;
use App\Models\VehicleExit;
use Illuminate\Support\Facades\DB;

/**
 * @mixin ExitTerminal
 */
trait ExitTerminalActions
{

    public function doVehicleExit(Customer $customer): VehicleExit
    {
        $ve = new VehicleExit(['exited_at' => now()]);
        $ve->exit_terminal()->associate($this);
        $ve->customer()->associate($customer);
        $ve->parameters = $customer->only(['plate', 'card_code', 'remaining_transits']);

        $entrance = $customer->getVehicleEntranceStillInside();
        if (empty($entrance)) {
            /** @var VehicleExit $oldExit */
            $oldExit = $customer->vehicle_exits()->latest('exited_at')->first();
            if (filled($oldExit) && $oldExit->exited_at->diffInRealSeconds($ve->exited_at) <= config('transit-charger.restore_exit_from_last_seconds')) {
                return $oldExit;
            }
            throw new VehicleNotEnteredException("Customer {$customer->debug_name} has not entered.");
        }

        $ve->vehicle_entrance()->associate($entrance);

        DB::transaction(function () use ($ve) {
            $ve->save();
        }, 3);

        return $ve;
    }
}
