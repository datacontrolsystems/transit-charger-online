<?php

namespace App\Models\Traits;


trait HasParameters
{

    /*

    protected $attributes = [
        'parameters' => '{}',
    ];
    protected $casts = [
        'parameters' => 'array',
    ];

    */

    /**
     * @param string $name
     * @param mixed  $value
     * @param string $attribute
     * @return $this
     */
    public function setParameter($name, $value, $attribute = 'parameters')
    {
        $p = $this->{$attribute};
        $this->{$attribute} = data_set($p, $name, $value);
        return $this;
    }

    /**
     * @param string $name
     * @param mixed  $default
     * @param string $attribute
     * @return mixed
     */
    public function getParameter($name, $default = null, $attribute = 'parameters')
    {
        return data_get($this->{$attribute}, $name, $default);
    }

}
