<?php

namespace App\Models\Traits;


trait HasDebugName
{

    /**
     * @return string
     */
    public function getDebugNameAttribute()
    {
        return "{$this->name} #" . ($this->id ?? 'new');
    }

}
