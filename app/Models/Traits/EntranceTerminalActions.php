<?php

namespace App\Models\Traits;

use App\Exceptions\InsufficientTransitsException;
use App\Exceptions\VehicleAlreadyInsideException;
use App\Models\Customer;
use App\Models\EntranceTerminal;
use App\Models\VehicleEntrance;
use Illuminate\Support\Facades\DB;

/**
 * @mixin EntranceTerminal
 */
trait EntranceTerminalActions
{

    public function doVehicleEntrance(Customer $customer): VehicleEntrance
    {
        $ve = new VehicleEntrance(['entered_at' => now()]);
        $ve->entrance_terminal()->associate($this);
        $ve->customer()->associate($customer);
        $ve->parameters = $customer->only(['plate', 'card_code', 'remaining_transits']);

        $oldEntrance = $customer->getVehicleEntranceStillInside();
        if ($oldEntrance) {
            if ($oldEntrance->entered_at->diffInRealSeconds($ve->entered_at) <= config('transit-charger.restore_entrance_from_last_seconds')) {
                return $oldEntrance;
            }
            throw new VehicleAlreadyInsideException("Customer {$customer->debug_name} is already inside.");
        }

        throw_unless($customer->remaining_transits > 0, InsufficientTransitsException::class, "Customer {$customer->debug_name} does not have remaining transits.");
        $customer->remaining_transits--;

        DB::transaction(function () use ($customer, $ve) {
            $ve->save();
            $customer->save();
        }, 3);

        return $ve;
    }

}
