<?php

namespace App\Models;

use App\Models\Traits\HasDebugName;
use App\Models\Traits\HasParameters;
use App\Models\Traits\ModelTraits;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Cashier;
use Stripe\Customer as StripeCustomer;

/**
 * App\Models\Customer
 *
 * @property int $id
 * @property string $name
 * @property string $card_code
 * @property string $plate
 * @property int $remaining_transits
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $tin
 * @property string|null $profession
 * @property string|null $tax_office
 * @property string|null $address
 * @property string|null $postal_code
 * @property string|null $city
 * @property string|null $state
 * @property array $parameters
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property string|null $stripe_id
 * @property string|null $pm_type
 * @property string|null $pm_last_four
 * @property \Illuminate\Support\Carbon|null $trial_ends_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Charge[] $charges
 * @property-read int|null $charges_count
 * @property-read \App\Models\User|null $creator
 * @property-read \App\Models\User|null $deleter
 * @property-read int $customer_id
 * @property-read string $debug_name
 * @property-read string $password
 * @property-read string $username
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Cashier\Subscription[] $subscriptions
 * @property-read int|null $subscriptions_count
 * @property-read \App\Models\User|null $updater
 * @property-read \App\Models\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VehicleEntrance[] $vehicle_entrances
 * @property-read int|null $vehicle_entrances_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VehicleExit[] $vehicle_exits
 * @property-read int|null $vehicle_exits_count
 * @method static \Illuminate\Database\Eloquent\Builder|Customer createdBy($userId)
 * @method static \Database\Factories\CustomerFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer newQuery()
 * @method static \Illuminate\Database\Query\Builder|Customer onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereCardCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereParameters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePlate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePmLastFour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePmType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereProfession($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereRemainingTransits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereStripeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereTaxOffice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereTin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereTrialEndsAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|Customer withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Customer withoutTrashed()
 * @mixin \Eloquent
 */
class Customer extends Model
{
    use HasFactory;
    use ModelTraits;
    use HasDebugName;
    use HasParameters;
    use Billable {
        createAsStripeCustomer as _createAsStripeCustomer;
    }

    protected $fillable = ['name', 'card_code', 'plate', 'phone', 'profession', 'tax_office', 'tin', 'address'];
    protected $hidden = ['stripe_id', 'pm_type', 'pm_last_four'];
    protected $attributes = [
        'parameters' => '{}',
    ];
    protected $casts = [
        'parameters' => 'array',
    ];
    protected $dates = ['trial_ends_at'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->allowPromotionCodes();
    }

    /**
     * @return $this
     */
    public function createUser(): Customer
    {
        throw_if($this->user, \Exception::class, "Cannot create user for customer {$this->debug_name} already bound to user " . optional($this->user)->debug_name);

        if ($this->id && ($u = User::whereCustomerId($this->id)->first())) {
            $this->user()->associate($u);
            return $this;
        }

        $u = new User($this->only(['name', 'username', 'email', 'phone', 'password']));
        //$u->customer_id = $this->id;
        $u->save();
        $u->assignRole(Role::customer());

        $this->user()->associate($u);

        return $this;
    }

    /**
     * @return $this
     */
    public function updateUser(): Customer
    {
        $attrs = ['name', 'username', 'email', 'phone', 'password'];

        if ($this->isDirty($attrs)) {
            $this->user->fill($this->only($attrs));
        }

        $this->user->customer()->associate($this);
        $this->user->save();

        return $this;
    }

    /**
     * @return $this
     */
    public function syncFromStripe(): Customer
    {
        $this->setParameter('stripe.customer', $this->createOrGetStripeCustomer()->jsonSerialize())
            ->save();
        return $this;
    }

    public function syncStripeCustomerDetails(): StripeCustomer
    {
        return $this->updateStripeCustomer([
            'name'     => $this->stripeName(),
            'email'    => $this->stripeEmail(),
            'phone'    => $this->stripePhone(),
            'address'  => $this->stripeAddress(),
            'metadata' => $this->stripeMetadata(),
        ]);
    }

    public function createAsStripeCustomer(array $options = []): StripeCustomer
    {
        return $this->_createAsStripeCustomer(array_merge([
            'metadata' => $this->stripeMetadata(),
        ], $options));
    }

    public function stripeMetadata(): array
    {
        return [
            'customer_id' => $this->id,
            'tin'         => $this->tin,
        ];
    }

    public function stripeAddress(): array
    {
        return [
            'country'     => 'GR',
            'city'        => $this->city,
            'line1'       => $this->address,
            'postal_code' => $this->postal_code,
            'state'       => $this->state,
        ];
    }

    public function getCustomerIdAttribute(): int
    {
        return $this->id;
    }

    public function getUsernameAttribute(): string
    {
        return $this->plate;
    }

    public function getPasswordAttribute(): string
    {
        return $this->card_code;
    }

    public function getVehicleEntranceStillInside(): ?VehicleEntrance
    {
        return $this->vehicle_entrances()->scopes('stillInside')->first();
    }

    public function charges(): HasMany
    {
        return $this->hasMany(Charge::class);
    }

    public function vehicle_entrances(): HasMany
    {
        return $this->hasMany(VehicleEntrance::class);
    }

    public function vehicle_exits(): HasMany
    {
        return $this->hasMany(VehicleExit::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->withoutGlobalScope(SoftDeletingScope::class);
    }

}
