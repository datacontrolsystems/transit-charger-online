<?php

namespace App\Models;

use App\Models\Traits\HasParameters;
use App\Models\Traits\ModelTraits;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\VehicleEntrance
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $entered_at
 * @property \Illuminate\Support\Carbon|null $exited_at
 * @property int $entrance_terminal_id
 * @property int $customer_id
 * @property array $parameters
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property bool|null $manually
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \App\Models\User|null $creator
 * @property-read \App\Models\Customer $customer
 * @property-read \App\Models\User|null $deleter
 * @property-read \App\Models\EntranceTerminal $entrance_terminal
 * @property-read \App\Models\User|null $updater
 * @property-read \App\Models\VehicleExit|null $vehicle_exit
 * @method static Builder|VehicleEntrance createdBy($userId)
 * @method static Builder|VehicleEntrance newModelQuery()
 * @method static Builder|VehicleEntrance newQuery()
 * @method static \Illuminate\Database\Query\Builder|VehicleEntrance onlyTrashed()
 * @method static Builder|VehicleEntrance query()
 * @method static Builder|VehicleEntrance stillInside()
 * @method static Builder|VehicleEntrance updatedBy($userId)
 * @method static Builder|VehicleEntrance whereCreatedAt($value)
 * @method static Builder|VehicleEntrance whereCreatedBy($value)
 * @method static Builder|VehicleEntrance whereCustomerId($value)
 * @method static Builder|VehicleEntrance whereDeletedAt($value)
 * @method static Builder|VehicleEntrance whereDeletedBy($value)
 * @method static Builder|VehicleEntrance whereEnteredAt($value)
 * @method static Builder|VehicleEntrance whereEntranceTerminalId($value)
 * @method static Builder|VehicleEntrance whereExitedAt($value)
 * @method static Builder|VehicleEntrance whereId($value)
 * @method static Builder|VehicleEntrance whereManually($value)
 * @method static Builder|VehicleEntrance whereParameters($value)
 * @method static Builder|VehicleEntrance whereUpdatedAt($value)
 * @method static Builder|VehicleEntrance whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|VehicleEntrance withTrashed()
 * @method static \Illuminate\Database\Query\Builder|VehicleEntrance withoutTrashed()
 * @mixin \Eloquent
 */
class VehicleEntrance extends Model
{
    use ModelTraits;
    use HasParameters;

    protected $fillable = ['entered_at'];
    protected $attributes = [
        'parameters' => '{}',
    ];
    protected $casts = [
        'parameters' => 'array',
        'manually'   => 'boolean',
    ];
    protected $dates = ['entered_at', 'exited_at'];

    public function scopeStillInside(Builder $q): Builder
    {
        return $q->whereNull('exited_at');
    }

    public function entrance_terminal(): BelongsTo
    {
        return $this->belongsTo(EntranceTerminal::class);
    }

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public function vehicle_exit(): HasOne
    {
        return $this->hasOne(VehicleExit::class);
    }

}
