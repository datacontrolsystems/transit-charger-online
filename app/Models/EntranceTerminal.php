<?php

namespace App\Models;

use App\Models\Traits\EntranceTerminalActions;
use App\Models\Traits\ModelCaching;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Parental\HasParent;

/**
 * App\Models\EntranceTerminal
 *
 * @property int $id
 * @property string $name
 * @property string|null $type
 * @property string|null $username
 * @property string|null $email
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $password
 * @property array $parameters
 * @property string|null $api_token
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property int|null $customer_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \App\Models\User|null $creator
 * @property-read \App\Models\Customer|null $customer
 * @property-read \App\Models\User|null $deleter
 * @property string|null $barrier_controller
 * @property-read string $debug_name
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-write mixed $plain_password
 * @property-read \App\Models\User|null $updater
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VehicleEntrance[] $vehicle_entrances
 * @property-read int|null $vehicle_entrances_count
 * @method static \Illuminate\Database\Eloquent\Builder|User createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal newQuery()
 * @method static \Illuminate\Database\Query\Builder|EntranceTerminal onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal query()
 * @method static \Illuminate\Database\Eloquent\Builder|User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereParameters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EntranceTerminal whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|EntranceTerminal withTrashed()
 * @method static \Illuminate\Database\Query\Builder|EntranceTerminal withoutTrashed()
 * @mixin \Eloquent
 */
class EntranceTerminal extends Terminal
{
    use HasParent;
    use EntranceTerminalActions;
    use ModelCaching;

    /**
     * @param $columns
     * @return EntranceTerminal[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function all($columns = ['*'])
    {
        return static::cache(__FUNCTION__, function () {
            return parent::all();
        });
    }

    public function vehicle_entrances(): HasMany
    {
        return $this->hasMany(VehicleEntrance::class, 'entrance_terminal_id');
    }

}
