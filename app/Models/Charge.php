<?php

namespace App\Models;

use App\Models\Traits\HasDebugName;
use App\Models\Traits\HasParameters;
use DigitalCloud\ModelNotes\HasNotes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Support\Collection;
use Laravel\Cashier\Cashier;
use Laravel\Cashier\Checkout;
use Laravel\Nova\Actions\Actionable;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * App\Models\Charge
 *
 * @property int $id
 * @property int $transits
 * @property string $payment_status
 * @property int $customer_id
 * @property string|null $stripe_checkout_id
 * @property array $parameters
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \App\Models\User|null $creator
 * @property-read \App\Models\Customer $customer
 * @property-read float|null $amount
 * @property-read string $debug_name
 * @property-read string $name
 * @property-read bool $payment_pending
 * @property-read bool $payment_success
 * @property-read \Illuminate\Database\Eloquent\Collection|\DigitalCloud\ModelNotes\Note[] $notes
 * @property-read int|null $notes_count
 * @property-read \App\Models\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|Charge createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Charge newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Charge newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Charge query()
 * @method static \Illuminate\Database\Eloquent\Builder|Charge updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Charge whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charge whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charge whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charge whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charge whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charge whereParameters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charge wherePaymentStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charge whereStripeCheckoutId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charge whereTransits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charge whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Charge whereUpdatedBy($value)
 * @mixin \Eloquent
 */
class Charge extends Model
{
    use Actionable;
    use BlameableTrait;
    use HasDebugName;
    use HasParameters;
    use HasNotes;

    public const PAYMENT_PENDING = 'pending';
    public const PAYMENT_SUCCESS = 'success';
    public const PAYMENT_CANCELLED = 'cancelled';
    public const PAYMENT_FAILED = 'failed';
    public const PAYMENT_STATUSES = [self::PAYMENT_PENDING, self::PAYMENT_SUCCESS, self::PAYMENT_CANCELLED, self::PAYMENT_FAILED];

    protected $fillable = ['transits', 'stripe_checkout_id'];
    protected $attributes = [
        'parameters'     => '{}',
        'payment_status' => self::PAYMENT_PENDING,
    ];
    protected $casts = [
        'parameters' => 'array',
    ];

    public static function listPaymentStatuses(): Collection
    {
        return collect(static::PAYMENT_STATUSES)->combine(self::PAYMENT_STATUSES)->map('humanize_attr');
    }

    public static function findByStripeCheckoutId(string $checkout_id): Charge
    {
        return static::where('stripe_checkout_id', $checkout_id)->firstOrFail();
    }

    public function getNameAttribute(): string
    {
        return $this->transits . " " . __('Transits');
    }

    public function getAmountAttribute($v): ?float
    {
        return $v ?? cents_to_eur($this->getParameter('stripe.checkout.amount_total'));
    }

    public function createStripeCheckout(): Checkout
    {
        throw_if($this->stripe_checkout_id, \Exception::class, "Charge {$this->debug_name} already create stripe checkout {$this->stripe_checkout_id}");

        $price = app('cashier-manager')->transitPrice();
        $product = app('cashier-manager')->transitProduct();
        $this->setParameter('stripe.price', $price->jsonSerialize())
            ->setParameter('stripe.product', $product->jsonSerialize());

        if (!$this->exists) {
            $this->save();
        }

        $checkout = $this->customer->checkout([$price->id => $this->transits], [
            'success_url'         => route('nova.customer.current') . '?session_id={CHECKOUT_SESSION_ID}&payment_status=' . self::PAYMENT_SUCCESS,
            'cancel_url'          => route('nova.customer.current') . '?session_id={CHECKOUT_SESSION_ID}&payment_status=' . self::PAYMENT_CANCELLED,
            'client_reference_id' => $this->id,
        ]);

        $this->stripe_checkout_id = $checkout->id;
        $this->syncFromStripe()->save();

        return $checkout;
    }

    /**
     * @return $this
     */
    public function syncFromStripe(): Charge
    {
        $checkout = Cashier::stripe()->checkout->sessions->retrieve($this->stripe_checkout_id);
        $payment = Cashier::stripe()->paymentIntents->retrieve($checkout->payment_intent);

        $this->setParameter('stripe.checkout', $checkout->jsonSerialize())
            ->setParameter('stripe.payment', $payment->jsonSerialize());

        if ($payment->status == "succeeded") {
            $this->payment_status = self::PAYMENT_SUCCESS;
        }

        return $this;
    }

    public function setPaymentStatusAttribute($status)
    {
        throw_unless(in_array($status, static::PAYMENT_STATUSES, true), \Exception::class, "Invalid payment status '$status'");
        return $this->attributes['payment_status'] = $status;
    }

    public function paymentStatus($status): bool
    {
        return $this->payment_status == $status;
    }

    public function getPaymentSuccessAttribute(): bool
    {
        return $this->paymentStatus(static::PAYMENT_SUCCESS);
    }

    public function getPaymentPendingAttribute(): bool
    {
        return $this->paymentStatus(self::PAYMENT_PENDING);
    }

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class)->withoutGlobalScope(SoftDeletingScope::class);
    }

}
