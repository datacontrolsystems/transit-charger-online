<?php

namespace App\Models;

use App\Exceptions\SocketException;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * App\Models\Terminal
 *
 * @property int $id
 * @property string $name
 * @property string|null $type
 * @property string|null $username
 * @property string|null $email
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $password
 * @property array $parameters
 * @property string|null $api_token
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property int|null $customer_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \App\Models\User|null $creator
 * @property-read \App\Models\Customer|null $customer
 * @property-read \App\Models\User|null $deleter
 * @property string|null $barrier_controller
 * @property-read string $debug_name
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-write mixed $plain_password
 * @property-read \App\Models\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|User createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal newQuery()
 * @method static \Illuminate\Database\Query\Builder|Terminal onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal query()
 * @method static \Illuminate\Database\Eloquent\Builder|User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereParameters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Terminal whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|Terminal withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Terminal withoutTrashed()
 * @mixin \Eloquent
 */
class Terminal extends User
{

    /**
     * @param array $columns
     * @return ExitTerminal[]|EntranceTerminal[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function all($columns = ['*'])
    {
        if (static::class == self::class) {
            return Cache::tags([EntranceTerminal::class, ExitTerminal::class])->remember('all', config('cache.duration'), function () {
                return EntranceTerminal::all()->merge(ExitTerminal::all());
            });
        }
        return parent::all();
    }

    /** @noinspection PhpHierarchyChecksInspection */
    /**
     * @param mixed $id
     * @param array $columns
     * @return EntranceTerminal|EntranceTerminal[]|ExitTerminal|ExitTerminal[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|object
     */
    public static function find($id, $columns = ['*'])
    {
        if ($id instanceof Model) {
            $id = $id->getKey();
        }else if ($id instanceof Collection) {
            $id = $id->modelKeys();
        }
        if (is_array($id) || $id instanceof Arrayable) {
            return EntranceTerminal::findMany($id, $columns)->merge(ExitTerminal::findMany($id, $columns));
        }
        return EntranceTerminal::whereKey($id)->first($columns) ?? ExitTerminal::whereKey($id)->first($columns);
    }

    /**
     * @return string|null
     */
    public function getBarrierControllerAttribute()
    {
        return $this->getParameter('barrier_controller');
    }

    /**
     * @param string|null $value
     * @return string|null
     */
    public function setBarrierControllerAttribute($value)
    {
        return $this->setParameter('barrier_controller', $value)->barrier_controller;
    }

    /**
     * @param bool $throwOnError
     * @return bool
     * @throws \Exception
     */
    public function openBarrier($throwOnError = false)
    {
        $remote_socket = $this->barrier_controller;
        if (blank($remote_socket))
            return false;
        try {
            return retry(2, function () use ($remote_socket) {
                $socket = stream_socket_client($remote_socket, $errno, $errstr, 1);
                throw_if($socket === false, SocketException::class, "Open socket error. open_barrier: {$this->debug_name} remote_socket: $remote_socket - $errstr: $errno", 1);
                $sent = fwrite($socket, "who=|$#lang=GRmsg=0@");
                throw_if($sent === false, SocketException::class, "Socket write error. open_barrier: {$this->debug_name} remote_socket: $remote_socket", 2);
                $closed = fclose($socket);
                throw_if($closed === false, SocketException::class, "Socket close error. open_barrier: {$this->debug_name} remote_socket: $remote_socket", 3);
                return true;
            }, 10);
        } catch (\ErrorException $e) {
            report($e);
            throw_if($throwOnError, new SocketException($e->getMessage(), 0, $e));
            return false;
        }
    }

}
