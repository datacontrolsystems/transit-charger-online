<?php

namespace App\Models;

use App\Models\Traits\HasParameters;
use App\Models\Traits\ModelTraits;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\VehicleExit
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $exited_at
 * @property int $exit_terminal_id
 * @property int $customer_id
 * @property int $vehicle_entrance_id
 * @property array $parameters
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property bool|null $manually
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \App\Models\User|null $creator
 * @property-read \App\Models\Customer $customer
 * @property-read \App\Models\User|null $deleter
 * @property-read \App\Models\ExitTerminal $exit_terminal
 * @property-read \App\Models\User|null $updater
 * @property-read \App\Models\VehicleEntrance $vehicle_entrance
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit newQuery()
 * @method static \Illuminate\Database\Query\Builder|VehicleExit onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit query()
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit whereExitTerminalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit whereExitedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit whereManually($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit whereParameters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleExit whereVehicleEntranceId($value)
 * @method static \Illuminate\Database\Query\Builder|VehicleExit withTrashed()
 * @method static \Illuminate\Database\Query\Builder|VehicleExit withoutTrashed()
 * @mixin \Eloquent
 */
class VehicleExit extends Model
{
    use ModelTraits;
    use HasParameters;

    protected $fillable = ['exited_at'];
    protected $attributes = [
        'parameters' => '{}',
    ];
    protected $casts = [
        'parameters' => 'array',
        'manually' => 'boolean',
    ];
    protected $dates = ['exited_at'];

    public function exit_terminal(): BelongsTo
    {
        return $this->belongsTo(ExitTerminal::class);
    }

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public function vehicle_entrance(): BelongsTo
    {
        return $this->belongsTo(VehicleEntrance::class);
    }

}
