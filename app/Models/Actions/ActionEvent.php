<?php

namespace App\Models\Actions;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Laravel\Nova\Actions\ActionEvent as NovaActionEvent;

/**
 * App\Models\Actions\ActionEvent
 *
 * @property int $id
 * @property string $batch_id
 * @property int $user_id
 * @property string $name
 * @property string $actionable_type
 * @property int $actionable_id
 * @property string $target_type
 * @property int $target_id
 * @property string $model_type
 * @property int|null $model_id
 * @property string $fields
 * @property string $status
 * @property string $exception
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property array|null $original
 * @property array|null $changes
 * @property-read Model|\Eloquent $target
 * @property-read \App\Models\User|null $user
 * @method static Builder|ActionEvent newModelQuery()
 * @method static Builder|ActionEvent newQuery()
 * @method static Builder|ActionEvent query()
 * @method static Builder|ActionEvent whereActionableId($value)
 * @method static Builder|ActionEvent whereActionableType($value)
 * @method static Builder|ActionEvent whereBatchId($value)
 * @method static Builder|ActionEvent whereChanges($value)
 * @method static Builder|ActionEvent whereCreatedAt($value)
 * @method static Builder|ActionEvent whereException($value)
 * @method static Builder|ActionEvent whereFields($value)
 * @method static Builder|ActionEvent whereId($value)
 * @method static Builder|ActionEvent whereModelId($value)
 * @method static Builder|ActionEvent whereModelType($value)
 * @method static Builder|ActionEvent whereName($value)
 * @method static Builder|ActionEvent whereOriginal($value)
 * @method static Builder|ActionEvent whereStatus($value)
 * @method static Builder|ActionEvent whereTargetId($value)
 * @method static Builder|ActionEvent whereTargetType($value)
 * @method static Builder|ActionEvent whereUpdatedAt($value)
 * @method static Builder|ActionEvent whereUserId($value)
 * @mixin \Eloquent
 */
class ActionEvent extends NovaActionEvent implements Scope
{
    /**
     * @var string|string[]
     */
    protected static $action_classes = [];
    /**
     * @var array
     */
    protected $fields_array = null;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new static);
    }

    public function getFields()
    {
        return $this->fields_array ?? ($this->fields_array = (unserialize($this->fields) ?: []));
    }

    public function getField($name)
    {
        return array_get($this->getFields(), $name);
    }

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param \Illuminate\Database\Eloquent\Model   $model
     * @return Builder
     */
    public function apply(Builder $builder, Model $model)
    {
        if (empty(static::$action_classes))
            return $builder;
        if (is_array(static::$action_classes)) {
            return (count(static::$action_classes) == 1) ?
                $builder->where('action', head(static::$action_classes)) :
                $builder->whereIn('action', static::$action_classes);
        }
        // else if is string
        return $builder->where('action', static::$action_classes);
    }

}
