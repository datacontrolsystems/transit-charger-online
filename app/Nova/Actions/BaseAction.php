<?php

namespace App\Nova\Actions;

use Closure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Actions\ActionEvent;
use Laravel\Nova\Actions\ActionMethod;
use Laravel\Nova\Actions\DispatchAction;
use Laravel\Nova\Exceptions\MissingActionHandlerException;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\ActionRequest;
use Laravel\Nova\Nova;

abstract class BaseAction extends Action
{
    public static $is_customer;

    protected $authorizeViaAbility = false;


    /**
     * @param Collection|Model[]|Model $models
     * @param array $fieldValues
     * @return mixed
     */
    public static function fire($models, $fieldValues = [])
    {
        Nova::resourcesIn(app_path('Nova'));
        $models = Collection::wrap($models);
        $fieldValues = Collection::wrap($fieldValues);
        $action = new static();
        $actionRequest = app(ActionRequest::class);
        $actionRequest->user = \Auth::user();
        $actionRequest->setUserResolver(function () {
            return \Auth::user();
        });
        $resource = Nova::resourceForModel($models->first());
//        $actionRequest->query->replace($fieldValues->toArray());
//        $actionRequest->replace($params);
        $actionRequest->route()->setParameter('resource', $resource::uriKey());
        $actionRequest->route()->pathInfo = "/nova-api/{$resource::uriKey()}/action";
        $actionRequest->query->set('resources', $models->map->getKey()->implode(','));
        $actionRequest->query->set('action', $action->uriKey());
        $actionRequest->query->set('pivotAction', 'false');
        $actionRequest->query->add($fieldValues->toArray());
//        return $action->handleRequest($actionRequest); // passes through permissions check. Problem for DeviceActionController
//        $method = ActionMethod::determine($action, $actionRequest->targetModel());
        $method = ActionMethod::determine($action, $resource);
        throw_unless(method_exists($action, $method), MissingActionHandlerException::make($action, $method));
        return DispatchAction::forModels($actionRequest, $action, $method, $models, $actionRequest->resolveFields());
    }

    /**
     * @param ActionFields $fields
     * @param Model $model
     * @return bool
     */
    public function updateFields($fields, $model)
    {
        return ActionEvent::where(['batch_id' => $this->batchId, 'target_id' => $model->getKey()])->update(['fields' => serialize($fields->getAttributes())]);
    }

    public function updateActionableType($models)
    {
        foreach ($models as $model) {
            ActionEvent::where(['batch_id' => $this->batchId, 'actionable_id' => $model->getKey()])->update(['actionable_type' => $c = get_class($model), 'target_type' => $c, 'model_type' => $c]);
        }
    }

    /**
     * @param Collection $models
     * @return mixed
     * @throws \Throwable Exception when count of models more than 1
     */
    protected function singleModel($models)
    {
        throw_if(count($models) != 1, \Exception::class, __('Please select only one record.'));
        return $models[0];
    }

    public function name()
    {
        return humanize($this);
    }

    public function authorizedToSee(Request $request)
    {
        return ($c = $this->getSeeCallback()) ? call_user_func($c, $request) : true;
    }

    /**
     * Determine if the action is executable for the given request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return bool
     */
    public function authorizedToRun(Request $request, $model)
    {
        return ($c = $this->getRunCallback()) ? call_user_func($c, $request, $model) : true;
    }

    /**
     * @return Closure|null
     */
    public function getRunCallback()
    {
        if ($this->authorizeViaAbility)
            return $this->getRunCallbackViaAbility();
        return $this->runCallback;
    }

    public function getRunCallbackViaAbility()
    {
        return function (Request $request, $model) {
            return $request->user()->can(class_basename($this) . " " . class_basename($model));
        };
    }

    /**
     * @return Closure|null
     */
    public function getSeeCallback()
    {
        if ($this->authorizeViaAbility) {
            return function (Request $request) {
                return $request->user()->can(class_basename($this) . " ");
            };
        }
        return $this->seeCallback;
    }

    public function uriKey()
    {
        return Str::kebab(str_replace('\\', '', static::class));
    }

}
