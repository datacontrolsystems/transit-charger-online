<?php

namespace App\Nova\Actions;

use App\Models\Role;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Select;

class CopyPermissionsFromRole extends BaseAction
{
    use InteractsWithQueue, Queueable, SerializesModels;
    public $onlyOnDetail = true;

    /**
     * @param \Laravel\Nova\Fields\ActionFields     $fields
     * @param \Illuminate\Support\Collection|Role[] $models
     * @return mixed
     */
    public function handleForRoles(ActionFields $fields, Collection $models)
    {
        $this->singleModel($models);
        $selectedRole = Role::findOrFail($fields->get('role'));
        foreach ($models as $m)
            $m->givePermissionTo($selectedRole->permissions);
    }

    /**
     * @param \Laravel\Nova\Fields\ActionFields     $fields
     * @param \Illuminate\Support\Collection|User[] $models
     * @return mixed
     */
    public function handleForUsers(ActionFields $fields, Collection $models)
    {
        $this->singleModel($models);
        $selectedRole = Role::findOrFail($fields->get('role'));
        foreach ($models as $m)
            $m->givePermissionTo($selectedRole->permissions);
    }

    public function fields()
    {
        return [
            Select::make(__('Role'), 'role')->options(Role::pluck('name', 'id')->toArray()),
        ];
    }

    public function getRunCallbackViaAbility()
    {
        return function (Request $request, $model) {
            return $request->user()->can('update Role');
        };
    }
}
