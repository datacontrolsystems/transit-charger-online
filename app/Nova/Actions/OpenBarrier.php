<?php

namespace App\Nova\Actions;

use App\Exceptions\BaseException;
use App\Models\EntranceTerminal;
use App\Models\ExitTerminal;
use App\Models\Terminal;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Select;

class OpenBarrier extends BaseAction
{
    use InteractsWithQueue, Queueable;

    public $showOnTableRow = true;
    public $showOnIndex = false;
    public $withoutConfirmation = false;

    public function __construct()
    {
        $this->canRun($c = fn(Request $request) => $request->user()->can('admin'));
        $this->canSee($c);
    }

    /**
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection|ExitTerminal[] $models
     * @return mixed
     */
    public function handleForExitTerminals(ActionFields $fields, Collection $models)
    {
        return $this->handleForTerminals($fields, $models);
    }

    /**
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection|EntranceTerminal[] $models
     * @return mixed
     */
    public function handleForEntranceTerminals(ActionFields $fields, Collection $models)
    {
        return $this->handleForTerminals($fields, $models);
    }

    /**
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection|Terminal[] $models
     * @return mixed
     */
    public function handleForTerminals(ActionFields $fields, Collection $models)
    {
        $terminal = $this->singleModel($models);
        $terminal = Terminal::find($terminal);
        $terminal->openBarrier(true);
        return Action::message(__('Opening Barrier'));
    }

    public function fields()
    {
        return [];
    }
}
