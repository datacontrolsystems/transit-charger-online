<?php

namespace App\Nova\Actions;

use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Number;

class CreditTransits extends BaseAction
{
    use InteractsWithQueue, Queueable;

    public function __construct()
    {
        $this->onlyOnDetail();
        $this->canSeeWhen('credit-transits');
    }

    /**
     * Perform the action on the given models.
     *
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        /** @var Customer $customer */
        $customer = $this->singleModel($models);
        $transits = $fields['transits'];

        $customer->remaining_transits += $transits;
        $customer->save();

        return Action::message("Νέο υπόλοιπο {$customer->remaining_transits} διελεύσεις");
    }

    public function fields()
    {
        return [
            Number::make(__('Transits'), 'transits')
                ->rules('required', 'integer', 'min:1')
                ->default(1)
                ->min(1)
                ->max(10)
                ->step(1),
        ];
    }
}
