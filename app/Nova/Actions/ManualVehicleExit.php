<?php

namespace App\Nova\Actions;

use App\Models\Customer;
use App\Models\ExitTerminal;
use App\Models\VehicleEntrance;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Select;

class ManualVehicleExit extends BaseAction
{
    use InteractsWithQueue, Queueable;

    /**
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection|VehicleEntrance[] $models
     * @return mixed
     */
    public function handleForVehicleEntrances(ActionFields $fields, Collection $models)
    {
        $this->singleModel($models);
        $exitTerminal = ExitTerminal::find($fields->get('exit_terminal'));
        foreach ($models as $entrance) {
            $exitTerminal->doVehicleExit($entrance->customer);
        }
        return Action::message(__('Success'));
    }

    /**
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection|Customer[] $models
     * @return mixed
     */
    public function handleForCustomers(ActionFields $fields, Collection $models)
    {
        $this->singleModel($models);
        $exitTerminal = ExitTerminal::find($fields->get('exit_terminal'));
        foreach ($models as $customer) {
            $exitTerminal->doVehicleExit($customer);
        }
        return Action::message(__('Success'));
    }

    public function fields()
    {
        return [
            Select::make(__('Exit Terminal'), 'exit_terminal')
                ->options(ExitTerminal::all()->pluck('name', 'id'))
                ->searchable()
                ->rules('required')->default(ExitTerminal::irregular()->id),
        ];
    }
}
