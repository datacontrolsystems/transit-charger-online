<?php

namespace App\Nova\Actions;

use App\Models\Charge;
use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Number;

class PurchaseTransits extends BaseAction
{
    use InteractsWithQueue, Queueable;

    public function __construct()
    {
        $this->confirmButtonText('Προς Πληρωμή');
        $this->onlyOnDetail();
        $this->canSee($authCallback = function (Request $request) {
            return static::$is_customer || $request->user()->can('super-admin');
        });
        $this->canRun($authCallback);
    }

    /**
     * Perform the action on the given models.
     *
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection|Customer[] $models
     * @return mixed
     */
    public function handleForCustomers(ActionFields $fields, Collection $models)
    {
        /** @var Customer $customer */
        $customer = $this->singleModel($models);
        $transits = $fields['transits'];

        $charge = new Charge(['transits' => $transits]);
        $charge->customer()->associate($customer);
        $checkout = $charge->createStripeCheckout();

        return Action::push('/stripe/checkout', ['id' => $checkout->id]);
    }

    public function fields()
    {
        return [
            Number::make(__('Transits'), 'transits')
                ->rules('required', 'integer', 'min:1', 'max:' . config('transit-charger.purchase.max'))
                ->min(1)
                ->max(config('transit-charger.purchase.max'))
                ->step(1)
                ->default(config('transit-charger.purchase.default')),
        ];
    }
}
