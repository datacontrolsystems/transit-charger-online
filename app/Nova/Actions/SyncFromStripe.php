<?php

namespace App\Nova\Actions;

use App\Models\Charge;
use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;

class SyncFromStripe extends Action
{
    use InteractsWithQueue, Queueable;

    public function __construct()
    {
        $this->withoutActionEvents();
        $this->canSeeWhen('super-admin');
    }

    /**
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection|Charge[] $models
     * @return mixed
     */
    public function handleForCharges(ActionFields $fields, Collection $models)
    {
        foreach ($models as $charge) {
            $charge->syncFromStripe()->save();
        }
    }

    /**
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection|Customer[] $models
     * @return mixed
     */
    public function handleForCustomers(ActionFields $fields, Collection $models)
    {
        foreach ($models as $customer) {
            $customer->syncFromStripe();
        }
    }



    public function fields()
    {
        return [];
    }
}
