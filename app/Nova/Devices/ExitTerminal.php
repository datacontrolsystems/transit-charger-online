<?php

namespace App\Nova\Devices;

use App\Nova\Actions\OpenBarrier;
use App\Nova\Resource;
use Gldrenthe89\NovaStringGeneratorField\NovaGenerateString;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Text;
use Sixlive\TextCopy\TextCopy;

class ExitTerminal extends Resource
{
    public static $model = \App\Models\ExitTerminal::class;
    public static $title = 'name';
    public static $globallySearchable = false;
    public static $search = [
        'id', 'name', 'email', 'username', 'phone',
    ];
    public static $perPageOptions = [50, 100, 150];
    public static $tableStyle = 'tight';

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make(__('Name'), 'name')
                ->sortable()
                ->rules('required', 'max:255'),
            Text::make(__('Username'), 'username')
                ->sortable()
                ->rules('required', 'max:254')
                ->creationRules('unique:users,username')
                ->updateRules('unique:users,username,{{resourceId}}'),
            Password::make(__('Password'), 'password')
                ->onlyOnForms()
                ->default('DataControlS')
                ->creationRules('required', 'string', 'min:6')
                ->updateRules('nullable', 'string', 'min:6'),
            TextCopy::make(__('API Token'), 'api_token')->truncate(10)->exceptOnForms()->canSeeWhen('super-admin'),
            NovaGenerateString::make(__('API Token'), 'api_token')->onlyOnForms()->canSeeWhen('super-admin')->length(60)->excludeRules(['symbols']),
            $this->jsonField(__('Parameters'), 'parameters')->canSeeWhen('super-admin'),
            Text::make(__('Barrier Controller'), 'barrier_controller')->help('<br>Example: <code>tcp://192.168.1.55:7001</code>'),
            HasMany::make(__('Vehicle Exits'), 'vehicle_exits', \App\Nova\Transits\VehicleExit::class),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function actions(Request $request)
    {
        return [
            OpenBarrier::make(),
            $this->actionDownloadExcel(),
        ];
    }
}
