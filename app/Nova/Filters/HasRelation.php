<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class HasRelation extends Filter
{
    /**
     * @var string name of relation
     */
    public $relation = null;

    public function __construct($relation)
    {
        $this->relation = $relation;
    }

    /**
     * @param \Illuminate\Http\Request              $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed                                 $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->{$value}($this->relation);
    }

    public function options(Request $request)
    {
        return [
            __("Yes") => 'has',
            __("No")  => 'doesntHave',
        ];
    }

    public function name()
    {
        return __("Has relation with") . ' ' . humanize_attr($this->relation);
    }

    public function key()
    {
        return get_class($this) . '-' . $this->relation;
    }
}
