<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Rela589n\NovaDateTimeFilter\NovaDateTimeFilter;

class CreatedAfter extends NovaDateTimeFilter
{
    public string $column = 'created_at';
    public $name;

    public function __construct($column = 'created_at', $name = null)
    {
        parent::__construct($column);
        $this->column = $column;
        $this->name = $name;
    }

    public function currentValue($value)
    {
        return $this->withMeta(['currentValue' => (string)$value]);
    }

    /**
     * Apply the filter to the given query.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        $value = Carbon::parse($value);
        return $query->where($this->column, '>=', $value);
    }

    /**
     * Set the date column which will be queried.
     * @param $column
     * @return $this
     */
    public function column($column)
    {
        $this->column = $column;
        return $this;
    }

    public function name()
    {
        return $this->name ?? (humanize_attr($this->column) . ' ' . __('After'));
    }

    public function key(): string
    {
        return static::class . '-' . $this->column;
    }
}
