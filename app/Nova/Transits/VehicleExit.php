<?php

namespace App\Nova\Transits;

use App\Nova\Filters\CreatedAfter;
use App\Nova\Filters\CreatedBefore;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;

class VehicleExit extends Resource
{
    public static $model = \App\Models\VehicleExit::class;
    public static $title = 'exited_at';
    public static $globallySearchable = false;
    public static $search = [
        'id', 'exited_at',
    ];
    public static $showPollingToggle = true;
    public static $with = ['customer', 'vehicle_entrance', 'exit_terminal'];
    public static $tableStyle = 'tight';

    public static function availableForNavigation(Request $request)
    {
        return $request->user()->can('admin');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            DateTime::make(__('Exited At'), 'exited_at')->exceptOnForms()->sortable(),
            $this->jsonField(__('Parameters'), 'parameters')->canSeeWhen('super-admin'),
            BelongsTo::make(__('Customer'), 'customer', \App\Nova\Transits\Customer::class)->withoutTrashed()->exceptOnForms(),
            BelongsTo::make(__('Vehicle Entrance'), 'vehicle_entrance', \App\Nova\Transits\VehicleEntrance::class)->withoutTrashed()->exceptOnForms(),
            BelongsTo::make(__('Exit Terminal'), 'exit_terminal', \App\Nova\Devices\ExitTerminal::class)->withoutTrashed()->exceptOnForms(),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function filters(Request $request)
    {
        return [
            new CreatedAfter('exited_at'),
            new CreatedBefore('exited_at'),
        ];
    }

    public function actions(Request $request)
    {
        return [
            $this->actionDownloadExcel(),
        ];
    }
}
