<?php

namespace App\Nova\Transits;

use App\Nova\Actions\ExportCharges;
use App\Nova\Actions\SyncFromStripe;
use App\Nova\Filters\CreatedAfter;
use App\Nova\Filters\CreatedBefore;
use App\Nova\Filters\SelectFilter;
use App\Nova\Lenses\PreviousMonthCharges;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Inspheric\Fields\Indicator;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Pdewit\ExternalUrl\ExternalUrl;

/**
 * @property \App\Models\Charge $resource
 * @mixin \App\Models\Charge
 */
class Charge extends Resource
{
    public static $model = \App\Models\Charge::class;
    public static $title = 'name';
    public static $globallySearchable = false;
    public static $search = [
        'id', 'charge', 'stripe_checkout_id',
    ];
    public static $showPollingToggle = true;
    public static $with = ['customer'];

    public static function availableForNavigation(Request $request)
    {
        return $request->user()->can('admin');
    }

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Indicator::make(__('Payment'), 'payment_status')
                ->labels(\App\Models\Charge::listPaymentStatuses()->toArray())
                ->colors([
                    \App\Models\Charge::PAYMENT_PENDING   => 'gray',
                    \App\Models\Charge::PAYMENT_CANCELLED => 'orange',
                    \App\Models\Charge::PAYMENT_FAILED    => 'red',
                    \App\Models\Charge::PAYMENT_SUCCESS   => 'green',
                ]),
            ExternalUrl::make(__('To Payment'), function () {
                return $this->getParameter('stripe.checkout.url');
            })
                ->onlyOnDetail()
                ->linkText(__('To Payment'))
                ->canSee(function () {
                    return !empty($this->getParameter('stripe.checkout.url'));
                }),
            Number::make(__('Transits'), 'transits')->exceptOnForms(),
            DateTime::make(__('Transits Credited'), function () {
                return $this->getParameter('transits_credited');
            }),
            Currency::make(__('Amount'), 'amount')->exceptOnForms(),
            ExternalUrl::make(__('Stripe Checkout Id'), function () {
                return 'https://dashboard.stripe.com/payments/' . $this->resource->getParameter('stripe.checkout.payment_intent');
            })
                ->linkText(__('Detailed'))
                ->onlyOnDetail()
                ->canSeeWhen('admin'),
            BelongsTo::make(__('Customer'), 'customer', \App\Nova\Transits\Customer::class)->withoutTrashed(),
            $this->parametersPanel(),
            $this->groupedBlameablePanel($request),
            $this->notesField()->canSeeWhen('admin'),
        ];
    }

//    public static function label()
//    {
//        return __('Purchases');
//    }
//
//    public static function singularLabel()
//    {
//        return __('Purchase');
//    }

    public function actions(Request $request)
    {
        return [
//            new ExportCharges(),
            new SyncFromStripe(),
            $this->actionDownloadExcel(),
        ];
    }

    public function filters(Request $request)
    {
        return [
            new SelectFilter(__('Payment'), 'payment_status', \App\Models\Charge::listPaymentStatuses()->flip()->toArray()),
            new CreatedAfter(),
            new CreatedBefore(),
        ];
    }

    public function lenses(Request $request)
    {
        return [
//            new PreviousMonthCharges(),
        ];
    }
}
