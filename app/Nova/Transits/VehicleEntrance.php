<?php

namespace App\Nova\Transits;

use App\Nova\Actions\ManualVehicleExit;
use App\Nova\Filters\CreatedAfter;
use App\Nova\Filters\CreatedBefore;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;

class VehicleEntrance extends Resource
{
    public static $model = \App\Models\VehicleEntrance::class;
    public static $title = 'entered_at';
    public static $globallySearchable = false;
    public static $search = [
        'id', 'entered_at',
    ];
    public static $showPollingToggle = true;
    public static $with = ['customer', 'entrance_terminal'];
    public static $tableStyle = 'tight';

    public static function availableForNavigation(Request $request)
    {
        return $request->user()->can('admin');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            DateTime::make(__('Entered At'), 'entered_at')->exceptOnForms()->sortable(),
            DateTime::make(__('Exited At'), 'exited_at')->exceptOnForms()->sortable(),
            $this->jsonField(__('Parameters'), 'parameters')->canSeeWhen('super-admin'),
            BelongsTo::make(__('Customer'), 'customer', \App\Nova\Transits\Customer::class)->exceptOnForms()->withoutTrashed(),
            HasOne::make(__('Vehicle Exit'), 'vehicle_exit', \App\Nova\Transits\VehicleExit::class),
            BelongsTo::make(__('Entrance Terminal'), 'entrance_terminal', \App\Nova\Devices\EntranceTerminal::class)->exceptOnForms()->withoutTrashed(),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function filters(Request $request)
    {
        return [
            new CreatedAfter('entered_at'),
            new CreatedBefore('entered_at'),
            new CreatedAfter('exited_at'),
            new CreatedBefore('exited_at'),
        ];
    }

    public function actions(Request $request)
    {
        return [
            new ManualVehicleExit(),
            $this->actionDownloadExcel(),
        ];
    }

}
