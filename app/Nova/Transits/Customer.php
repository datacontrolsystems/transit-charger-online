<?php

namespace App\Nova\Transits;

use App\Nova\Actions\CreditTransits;
use App\Nova\Actions\PurchaseTransits;
use App\Nova\Actions\SyncFromStripe;
use App\Nova\Resource;
use App\Support\TaxOffices;
use Illuminate\Http\Request;
use Khalin\Nova\Field\Link;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Panel;
use Lednerb\TextLinked\TextLinked;
use Sixlive\TextCopy\TextCopy;

/**
 * @property \App\Models\Customer $resource
 * @mixin \App\Models\Customer
 */
class Customer extends Resource
{
    public static $model = \App\Models\Customer::class;
    public static $title = 'name';
    public static $subtitle = 'plate';
    public static $search = [
        'id', 'name', 'phone', 'email', 'plate', 'card_code', 'tin', 'stripe_id',
    ];
    public static $with = ['user'];
    public static $preventFormAbandonment = true;

    public static function availableForNavigation(Request $request)
    {
        return $request->user()->can('admin');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request|NovaRequest $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make(__('Name'), 'name')->rules('required'),
            Text::make(__('Phone'), 'phone')
                ->creationRules(['nullable', 'unique:customers,phone'])
                ->updateRules(['nullable', 'unique:customers,phone,{{resourceId}}']),
            TextCopy::make(__('Email'), 'email')->rules(['nullable', 'email'])
                ->creationRules(['nullable', 'unique:customers,email'])
                ->updateRules(['nullable', 'unique:customers,email,{{resourceId}}']),
            BelongsTo::make(__('User'), 'user', \App\Nova\Management\User::class)->exceptOnForms(),

            new Panel(__('Transits'), [
                Number::make(__('Remaining Transits'), 'remaining_transits')->exceptOnForms()->sortable(),
                Number::make(__('Remaining Transits'), 'remaining_transits')->onlyOnForms()
                    ->rules('required', 'integer', 'min:0')
                    ->canSeeWhen('super-admin'),
                TextCopy::make(__('Plate'), 'plate')
                    ->rules('required')
                    ->creationRules('unique:customers,plate')
                    ->updateRules('unique:customers,plate,{{resourceId}}')
                    ->help('Username / Όνομα Χρήστη'),
                TextCopy::make(__('Card Code'), 'card_code')
                    ->creationRules(['nullable', 'unique:customers,card_code'])
                    ->updateRules(['nullable', 'unique:customers,card_code,{{resourceId}}'])
                    ->help('Password / Κωδικός Εισόδου'),
            ]),

            new Panel(__('Billing'), [
                Text::make(__('Address'), 'address')->hideFromIndex(),
                Select::make(__('Tax Office'), 'tax_office')->searchable()->displayUsingLabels()->options(TaxOffices::list())->hideFromIndex(),
                Text::make(__('Profession'), 'profession')->hideFromIndex(),
                TextCopy::make(__('TIN'), 'tin')
                    ->rules(['nullable', 'digits:9'])
                    ->creationRules(['nullable', 'unique:customers,tin'])
                    ->updateRules(['nullable', 'unique:customers,tin,{{resourceId}}']),
                Link::make(__('Stripe Id'), 'stripe_id')
                    ->url(function () {
                        return "https://dashboard.stripe.com/customers/{$this->stripe_id}";
                    })
                    ->blank()
                    ->exceptOnForms()
                    ->canSeeWhen('admin'),
                //Text::make(__('Payment Method Last Four'), 'pm_last_four')->exceptOnForms(),
            ]),

            HasMany::make(__('Charges'), 'charges', \App\Nova\Transits\Charge::class),
            HasMany::make(__('Vehicle Entrances'), 'vehicle_entrances', \App\Nova\Transits\VehicleEntrance::class),
            HasMany::make(__('Vehicle Exits'), 'vehicle_exits', \App\Nova\Transits\VehicleExit::class)
                ->canSeeWhen('admin'),
            $this->parametersPanel(),
            $this->groupedBlameablePanel($request)
        ];
    }

    public function actions(Request $request)
    {
        return [
            new CreditTransits(),
            PurchaseTransits::make(),
            SyncFromStripe::make()->canSeeWhen('admin'),
            $this->actionDownloadExcel()
                ->canSee(function (Request $request) {
                    return !static::$is_customer;
                }),
        ];
    }

}
