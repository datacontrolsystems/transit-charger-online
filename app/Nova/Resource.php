<?php

namespace App\Nova;

use App\Nova\Filters\CreatedAfter;
use App\Nova\Management\User;
use DigitalCloud\NovaResourceNotes\Fields\Notes;
use Dillingham\NovaGroupedField\Grouped;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Code;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Nova;
use Laravel\Nova\Panel;
use Laravel\Nova\Resource as NovaResource;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

abstract class Resource extends NovaResource
{
    public static $group = 'Other';

    public static $subtitle;

    public static $is_customer;

    public function subtitle()
    {
        return static::$subtitle ? $this->{static::$subtitle} : null;
    }

    public static function group()
    {
        return __(humanize(static::$group == 'Other' ? static::getDirPath() : static::$group));
    }

    public static function getDirPath()
    {
        return str_before(str_after(static::class, 'App\\Nova\\'), '\\');
    }

    protected function timestampablePanel(Request $request)
    {
        return (new Panel(__('Timestamps'), [
            DateTime::make(__('Created At'), 'created_at')->sortable()->exceptOnForms(),
            DateTime::make(__('Updated At'), 'updated_at')->sortable()->exceptOnForms(),
            DateTime::make(__('Deleted At'), 'deleted_at')->sortable()->exceptOnForms()->canSee(function () {
                return $this->deleted_at;
            }),
        ]));
    }

    protected function blameablePanel(Request $request)
    {
        return new Panel(__('Handlers'), [
            BelongsTo::make(__('Creator'), 'creator', User::class)->exceptOnForms()->hideFromIndex(),
            BelongsTo::make(__('Updater'), 'updater', User::class)->exceptOnForms()->hideFromIndex(),
            BelongsTo::make(__('Deleter'), 'deleter', User::class)->exceptOnForms()->hideFromIndex()->canSee(function () {
                return $this->deleted_at;
            }),
        ]);
    }

    /**
     * @param Request $request
     * @return Panel
     */
    protected function groupedBlameablePanel(Request $request)
    {
        $separator = '-';
        return new Panel(__('Timestamps') . " $separator " . __('Handlers'), [
            DateTime::make(__('Created At'), 'created_at')->sortable()->onlyOnIndex(),
//            DateTime::make(__('Updated At'), 'updated_at')->sortable()->onlyOnIndex(),
            DateTime::make(__('Deleted At'), 'deleted_at')->sortable()->onlyOnIndex()->canSee(function () {
                return $this->deleted_at;
            }),
            /***/
            Grouped::make(__('Created'))->fields([
                DateTime::make(__('Created At'), 'created_at'),
                BelongsTo::make(__('Creator'), 'creator', User::class),
            ])->hideFromIndex()->separator($separator),
            Grouped::make(__('Updated'))->fields([
                DateTime::make(__('Updated At'), 'updated_at'),
                BelongsTo::make(__('Updater'), 'updater', User::class),
            ])->hideFromIndex()->separator($separator),
            Grouped::make(__('Deleted'))->fields([
                DateTime::make(__('Deleted At'), 'deleted_at'),
                BelongsTo::make(__('Deleter'), 'deleter', User::class),
            ])->hideFromIndex()->separator($separator)->canSee(function () {
                return $this->deleted_at;
            }),
        ]);
    }

    protected function resourceLinkField($name, ?Model $model)
    {
        return Text::make((empty($model) ? $name : __(humanize($model))), function () use ($model) {
            if (empty($model))
                return '-';
            $classPath = kebab_case(str_plural(class_basename($model)));
            return '<a href="' . url(config('nova.path') . '/resources/' . $classPath . '/' . $model->getKey()) . '" class="no-underline font-bold dim text-primary">' . ($model->name ?? $model->getKey()) . '</a>';
        })->exceptOnForms()->asHtml();
    }

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __(str_plural(static::resourceName()));
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __(static::resourceName());
    }

    public static function resourceName()
    {
        return Nova::humanize(class_basename(static::class));
    }

    public function actions(Request $request)
    {
        return [
            $this->actionDownloadExcel(),
        ];
    }

    /**
     * @return DownloadExcel
     */
    public function actionDownloadExcel()
    {
        return (new DownloadExcel())->withHeadings();
    }

    public function filters(Request $request)
    {
        return [
            new CreatedAfter(),
        ];
    }

    /**
     * @return Notes
     */
    public function notesField(): Notes
    {
        return Notes::make(__('Notes'), 'notes')->onlyOnDetail();
    }

    /**
     * @param string $name
     * @param string $attr
     * @return Code
     */
    public function jsonField($name, $attr)
    {
        return Code::make($name, $attr)->rules('json')->autoHeight()
            ->json(JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_LINE_TERMINATORS | JSON_UNESCAPED_SLASHES | JSON_THROW_ON_ERROR | JSON_FORCE_OBJECT);
    }

    public function parametersField(): Code
    {
        return $this->jsonField(__('Parameters'), 'parameters')->canSeeWhen('super-admin');
    }

    public function parametersPanel(): Panel
    {
        return new Panel(__('Parameters'), [
            $this->parametersField(),
        ]);
    }

    public function currencyField($name, $attr): Currency
    {
        return Currency::make($name, $attr)
            ->context(new \Brick\Money\Context\AutoContext());
    }

}
