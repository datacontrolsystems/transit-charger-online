<?php

namespace App\Scopes;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class UserTypeScope implements Scope
{

    /**
     * @param Builder $builder
     * @param User $model
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->where($model->getInheritanceColumn(), $model->classToAlias(get_class($model)));
    }
}
