<?php

namespace App\Imports;

use App\Models\ExitTerminal;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ExitTerminalsImport extends GenericModelImport
{
    protected $model = \App\Models\ExitTerminal::class;
}
