<?php

namespace App\Imports;

class EntranceTerminalsImport extends GenericModelImport
{
    protected $model = \App\Models\EntranceTerminal::class;
}
