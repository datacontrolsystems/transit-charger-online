<?php

namespace App\Imports;

class CustomersImport extends GenericModelImport
{
    protected $model = \App\Models\Customer::class;
}
