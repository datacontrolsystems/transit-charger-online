<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class TaxOfficesImport implements WithHeadingRow, WithCustomCsvSettings
{
    use Importable;

    public function getCsvSettings(): array
    {
        return [
            //'input_encoding'   => 'ISO-8859-1',
            //'escape_character' => '""',
            //'enclosure'        => '"',
            'delimiter'        => ';',
        ];
    }

}
