<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class GenericModelImport implements ToModel, WithHeadingRow
{
    use Importable;

    protected $model;

    public function model(array $row)
    {
        return new $this->model($row);
    }
}
