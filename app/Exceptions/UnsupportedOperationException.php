<?php
/**
 * UnsupportedOperationException.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 16/11/2018 09:10
 */

namespace App\Exceptions;

use RuntimeException;

class UnsupportedOperationException extends RuntimeException
{

}
