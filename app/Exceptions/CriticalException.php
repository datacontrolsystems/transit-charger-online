<?php

namespace App\Exceptions;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class CriticalException extends BaseException
{
    /**
     * @return bool|null
     */
    public function report()
    {
        if ($p = $this->getPrevious()) {
            report($p);
        }
        if (App::environment('production')) {
            Log::critical("Critical Exception: {$this->getMessage()}", $this->toArray());
        }
        return null;
    }
}
