<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Console\Kernel as ConsoleKernelContract;
use Illuminate\Support\Collection;

class AppInstall extends Command
{
    protected $signature = 'app:install {--r|reset : Drop tables}';

    protected $description = 'Create database, seed default models & configuration, clear caches';

    public function handle()
    {
        $this->call('clear:caches');
        $this->getFreshConfiguration();
        $reset = $this->option('reset');
        if (app('installed')) {
            if ($reset) {
                $this->warn('App already installed at ' . config('app.installed_at'));
            } else {
                $this->error('App already installed at ' . config('app.installed_at'));
                return;
            }
        }
        if ($reset) {
            $this->resetApp();
        }
        $this->getFreshConfiguration();
        $this->call('migrate', ['--force' => true]);
        $this->call('db:seed', ['--force' => true]);
        $this->callSilent('storage:link');
        $this->call('nova:combine-tools');
        if (app()->environment('local'))
            $this->call('ide-helper:all', ['-q' => true]);
        $this->call('env');
    }

    protected function resetApp()
    {
        if (app()->environment() != 'local') {
            rescue(function () {
                $this->call('snapshot:create', ['name' => 'reset-' . now()->format('Y-m-d_H-i-s')]);
            });
        }
        $this->dropDB('mysql');
        $this->dropDB('telescope');
    }

    protected function dropDB($database)
    {
        $this->laravel['db']->connection($database)->getSchemaBuilder()->dropAllTables();
        $this->laravel['db']->connection($database)->getSchemaBuilder()->dropAllViews();
        $this->info("Dropped all tables/views successfully on $database.");
    }

    protected function getFreshConfiguration($load = true)
    {
        $app = require $this->laravel->bootstrapPath() . '/app.php';
        $app->make(ConsoleKernelContract::class)->bootstrap();
        if ($load) {
            config($app['config']->all());
        }
        return $app['config']->all();
    }

    public static function migrated()
    {
        $migrator = app('migrator');
        if (!$migrator->repositoryExists())
            return false;
        $ran = $migrator->getRepository()->getRan();
        $paths = array_merge($migrator->paths(), [database_path() . DIRECTORY_SEPARATOR . 'migrations']);
        $migrationFiles = $migrator->getMigrationFiles($paths);
        $migrations = Collection::make($migrationFiles)->mapWithKeys(function ($migrationFile) use ($migrator, $ran) {
            return [$migrationFile => $migrator->getMigrationName($migrationFile)];
        });
        $notRan = $migrations->reject(function ($migrationName) use ($ran) {
            return in_array($migrationName, $ran);
        });
        return $notRan->isEmpty();
    }
}
