<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\App;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        //
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('telescope:prune --hours=120')->daily();
        $schedule->command('horizon:snapshot')->everyFiveMinutes();
        $schedule->command('backup:run')->skip(App::environment('local'))->daily()->at('02:00');
        $schedule->command('backup:clean')->skip(App::environment('local'))->daily()->at('03:00');
        $schedule->command('backup:monitor')->skip(App::environment('local'))->daily()->at('04:00');
    }

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
