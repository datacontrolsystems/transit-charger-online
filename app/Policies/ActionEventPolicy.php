<?php

namespace App\Policies;

use App\Models\User;

class ActionEventPolicy extends BasePolicy
{

    public function create(User $user)
    {
        return false;
    }

    public function update(User $user, $model)
    {
        return false;
    }

    public function delete(User $user, $model)
    {
        return false;
    }

    public function restore(User $user, $model)
    {
        return false;
    }
}
