<?php

namespace App\Policies;

use App\Models\ExitTerminal;
use App\Models\User;

class ExitTerminalPolicy extends BasePolicy
{
    /**
     * @param User $user
     * @param ExitTerminal $exitTerminal
     * @return bool
     */
    public function delete(User $user, $exitTerminal)
    {
        return $exitTerminal->username != 'irregularExitTerminal' && parent::delete($user, $exitTerminal);
    }

}
