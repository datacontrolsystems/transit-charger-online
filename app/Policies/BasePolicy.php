<?php

namespace App\Policies;


use App\Models\User;

abstract class BasePolicy
{
    use PolicyTrait;

    protected function sameCustomer(User $user, $model)
    {
        return $user->customer_id == $model->customer_id;
    }
}
