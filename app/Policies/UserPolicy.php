<?php

namespace App\Policies;

use App\Models\User;

class UserPolicy extends BasePolicy
{
    /**
     * @param User $user
     * @param User $model
     * @return array|bool|mixed
     */
    public function delete(User $user, $model)
    {
        return parent::delete($user, $model) && !$model->isCritical();
    }

}
