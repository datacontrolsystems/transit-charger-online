<?php

namespace App\Policies;

use App\Models\User;
use App\Models\VehicleEntrance;

class VehicleEntrancePolicy extends BasePolicy
{

    /**
     * @param User $user
     * @param VehicleEntrance $model
     * @return bool|void
     */
    public function view(User $user, $model)
    {
        return parent::view($user, $model) || $this->sameCustomer($user, $model);
    }

}
