<?php

namespace App\Policies;

use App\Models\User;
use App\Models\VehicleExit;

class VehicleExitPolicy extends BasePolicy
{

    /**
     * @param User $user
     * @param VehicleExit $model
     * @return bool|void
     */
    public function view(User $user, $model)
    {
        return parent::view($user, $model) || $this->sameCustomer($user, $model);
    }

}
