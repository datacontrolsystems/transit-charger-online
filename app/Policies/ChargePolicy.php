<?php

namespace App\Policies;

use App\Models\Charge;
use App\Models\User;

class ChargePolicy extends BasePolicy
{

    /**
     * @param User $user
     * @param Charge $model
     * @return bool|void
     */
    public function view(User $user, $model)
    {
        return parent::view($user, $model) || $this->sameCustomer($user, $model);
    }

    /**
     * @param User $user
     * @param Charge $model
     * @return bool
     */
    public function delete(User $user, $model)
    {
        return parent::delete($user, $model) && !$model->payment_success;
    }

}
