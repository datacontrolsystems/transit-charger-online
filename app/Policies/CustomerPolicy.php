<?php

namespace App\Policies;

use App\Models\Customer;
use App\Models\User;

class CustomerPolicy extends BasePolicy
{

    /**
     * @param User $user
     * @param Customer $model
     * @return bool
     */
    public function view(User $user, $model)
    {
        return parent::view($user, $model) || $this->sameCustomer($user, $model);
    }

}
