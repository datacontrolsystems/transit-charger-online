<?php

namespace Theograms\NovaStripeCheckout;

use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;

class AssetServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Nova::serving(function (ServingNova $event) {
            Nova::script('nova-stripe-checkout', __DIR__.'/../dist/js/asset.js');
            Nova::style('nova-stripe-checkout', __DIR__.'/../dist/css/asset.css');
            Nova::provideToScript([
                'stripe' => [
                    'key' => config('cashier.key'),
                ],
            ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
